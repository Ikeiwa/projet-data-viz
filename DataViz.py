# -*- coding: utf-8 -*-
import sys
import os
from pyvis.network import Network
from bs4 import BeautifulSoup
from PyQt5.QtWidgets import QApplication, QMainWindow, QDialog, QLineEdit, QMessageBox
from PyQt5.QtWebEngineWidgets import QWebEngineView
from PyQt5.QtCore import QUrl


from UI_graph import Ui_GraphWindow
from UI_request import Ui_RequestWindow
from UI_list import Ui_ListWindow
from RequestManager import RequestManager
from PandasDFModel import DataFrameModel

mainDir = os.path.dirname(os.path.realpath(__file__))

class ListDialog(QDialog):
    def __init__(self, elementList, *args, **kwargs):
        super(ListDialog, self).__init__(*args, **kwargs)
        self.ui = Ui_ListWindow()
        self.ui.setupUi(self)
        self.elementList = elementList
        for elem in self.elementList:
            textItem = QLineEdit(elem)
            self.ui.ElementLayout.addWidget(textItem)
            
        self.ui.AddButton.clicked.connect(self.AddElement)
        self.ui.RemoveButton.clicked.connect(self.RemoveElement)
        
    def GetResults(self):
        if self.exec_() == QDialog.Accepted:
            return self.elementList
        else:
            return None
    
    def AddElement(self):
        textItem = QLineEdit()
        self.ui.ElementLayout.insertWidget(0,textItem)
        
    def RemoveElement(self):
        self.ui.ElementLayout.takeAt(0)

class GraphWindow(QMainWindow):
    def __init__(self, *args, **kwargs):
        super(GraphWindow, self).__init__(*args, **kwargs)
        self.ui = Ui_GraphWindow()
        self.ui.setupUi(self)
        self.webview = QWebEngineView()
        self.ui.GraphLayout.addWidget(self.webview)
        self.graphManager = GraphManager(self.webview)
        self.ui.actionCapturer.triggered.connect(self.SaveScreen)
        
    def SaveScreen(self):
        self.webview.grab().save("graph.png")
        msg = QMessageBox()
        msg.setWindowTitle("Info")
        msg.setText("Graph capturé avec succès")
        msg.exec_()
        
class RequestWindow(QMainWindow):
    def __init__(self, graphWindow, *args, **kwargs):
        super(RequestWindow, self).__init__(*args, **kwargs)
        self.ui = Ui_RequestWindow()
        self.ui.setupUi(self)
        self.requestManager = RequestManager()
        self.graphWindow = graphWindow
        
        self.ui.PreviewButton.clicked.connect(self.PreviewRequest)
        self.ui.ShowGraphButton.clicked.connect(self.ShowGraph)
        
        self.listVenues = []
        
    def ExecuteRequest(self):
        
        singleElement = self.ui.SingleSelectorInput.text()
        # if self.ui.RButtonNameSelect.isChecked():
        #     if self.ui.TableSelector.currentIndex() > 0:
        #         singleElement = self.requestManager.getAuteurIdFromName(singleElement)
            
        if self.ui.LimitResultCheck.isChecked():
            self.requestManager.maxLimit = self.ui.MaxResultSelector.value()
        else:
            self.requestManager.maxLimit = 500
        
        if self.ui.TableSelector.currentIndex() == 0:
            
            coAutor = self.ui.AutorCheck.isChecked()
            publication = self.ui.PublicationCheck.isChecked()
            keyword = self.ui.KeywordCheck.isChecked()
            venue = self.ui.VenueCheck.isChecked()
            
            if coAutor and not publication and not keyword and not venue:
                self.requestManager.selectCoAuteurFromAuteur(singleElement)
                return True
            
            if publication and not coAutor and not keyword and not venue:
                self.requestManager.selectPublicationFromAuteur(singleElement)
                return True
            
            if keyword and not publication and not coAutor and not venue :
                self.requestManager.selectMotsClesFromAuteur(singleElement)
                return True
            
            if coAutor and publication and not keyword and not venue :
                self.requestManager. selectCoauteurPublicationFromAuteur(singleElement)
                return True
            
            if coAutor and not publication and keyword and not venue :
                self.requestManager.selectCoauteurKeywordFromAuteur(singleElement)
                return True
            
            if not coAutor and publication and keyword and not venue:
                self.requestManager.selectMotsClesAndPublicationsFromAuteur(singleElement)
                return True
            
            if coAutor and publication and keyword and not venue :
                self.requestManager.selectCoauteurPublicationKeywordFromAuteur(singleElement)
                return True
            
            if not coAutor and not publication and not keyword and not venue:
                msg = QMessageBox()
                msg.setWindowTitle("Graph Impossible")
                msg.setText(self.requestManager.getInfoAuteur(singleElement))
                msg.exec_()
                return False
             
            if coAutor and not publication and not keyword and  venue:
                self.requestManager.selectCoauteurVenueFromAuteur(singleElement)
                return True
            
            if publication and not coAutor and not keyword and  venue:
                self.requestManager.selectPublicationVenueParAuteur(singleElement)
                return True
            
            if keyword and not publication and not coAutor and  venue :
                self.requestManager.selectKeywordVenueFromAuteur(singleElement)
                return True
            
            if coAutor and publication and not keyword and  venue :
                self.requestManager.selectCoauteurPublicationVenueFromAuteur(singleElement)
                return True 
            
            if coAutor and not publication and keyword and  venue :
                self.requestManager.selectCoauteurKeywordVenueFromAuteur(singleElement)
                return True
            if not coAutor and publication and keyword and  venue:
                #self.requestManager.selectPublicationKeywordVenueFromAuteur(singleElement)
                return False
            
            if coAutor and publication and keyword and  venue :
                self.requestManager.selectgraphfinal(singleElement)
                return True
            
            if not coAutor and not publication and not keyword and venue:
                self.requestManager.selectVenueParAuteur(singleElement)
                return True           
            
        elif self.ui.TableSelector.currentIndex() == 1:
            if self.ui.RButtonNameSelect.isChecked() and len(singleElement) > 0:
                singleElement = self.requestManager.getPubidFromTitle(singleElement)
            auteur = self.ui.AutorCheck_2.isChecked()
            keyword = self.ui.KeywordCheck_2.isChecked()
            venue = self.ui.VenueCheck_2.isChecked() 
            year =self.ui.YearCheck.isChecked()
            if not auteur and not keyword and not venue and not year :
                msg = QMessageBox()
                msg.setWindowTitle("Graph Impossible")
                msg.setText(self.requestManager.getInfoPublication(singleElement))
                msg.exec_()
                return False
            if not auteur and not keyword and  venue and not year :
                self.requestManager.selectVenueFromPublication(singleElement)
                return True
            if not auteur and  keyword and not venue and not year :
                self.requestmanager.selectKeywordFromPublication(singleElement)
                return True
            if not auteur and  keyword and  venue and not year :
                return
            if  auteur and not keyword and not venue and not year :
                self.requestManager.selectAuteurFromPublication(singleElement)
                return True
            if  auteur and not keyword and  venue and not year :
                return
            if  auteur and  keyword and not venue and not year :
                return
            if  auteur and  keyword and  venue and not year :
                return  
            
            
            
            if not auteur and not keyword and not venue and  year :
                self.requestManager.selectYearFromPublication(singleElement)
                return True
            if not auteur and not keyword and  venue and  year :
                return
            if not auteur and  keyword and not venue and  year :
                return
            if not auteur and  keyword and  venue and  year :
                return
            if  auteur and not keyword and not venue and  year :
                return
            if  auteur and not keyword and  venue and  year :
                return
            if  auteur and  keyword and not venue and  year :
                return
            if  auteur and  keyword and  venue and  year :
                return  
            
        elif self.ui.TableSelector.currentIndex() == 2:
            if self.ui.RButtonNameSelect.isChecked() and len(singleElement) > 0:
                singleElement = self.requestManager.getVenueIdFromName(singleElement)
            auteur = self.ui.AutorCheck_3.isChecked()
            keyword = self.ui.KeywordCheck_3.isChecked()
            publication = self.ui.PublicationCheck_3.isChecked()
            if not auteur and not keyword and not publication :
                msg = QMessageBox()
                msg.setWindowTitle("Graph Impossible")
                msg.setText(self.requestManager.getInfoVenue(singleElement))
                msg.exec_()
                return False
            if not auteur and not keyword and publication :
                self.requestManager.selectPublicationFromVenue(singleElement)
                return True
            if not auteur and keyword and not publication :
                self.requestManager.selectkeywordFromVenue(singleElement)
                return True
            if not auteur and keyword and publication :
                self.requestManager.selectKeywordPublicationFromVenue(singleElement)
                return True
            if auteur and not keyword and not publication :
                self.requestManager.selectAuteurFromVenue(singleElement)
                return True
            if  auteur and not keyword and publication :
                self.requestManager.selectAuteurPublicationFromVenue(singleElement)
                return True
            if  auteur and keyword and not publication :
                self.requestManager.selectAuteurKeywordFromVenue(singleElement)
                return True
            if  auteur and keyword and publication :
                self.requestManager.selectAuteurKeywordPublicationFromVenue(singleElement)
                return True
            
            
        elif self.ui.TableSelector.currentIndex() == 3:
            auteur = self.ui.AutorCheck_4.isChecked()
            publication = self.ui.PublicationCheck_4.isChecked()
            venue = self.ui.VenueCheck_4.isChecked()
            if not auteur and not venue and not publication :
                msg = QMessageBox()
                msg.setWindowTitle("Graph Impossible")
                msg.setText(self.requestManager.getInfoKeyword(singleElement))
                msg.exec_()
                return False
            if not auteur and not venue and publication :
                self.requestManager.selectPublicationFromKeyword(singleElement)
                return True
            if not auteur and venue and not publication :
                self.requestManager.selectVenueFromKeyword(singleElement)
                return True
            if not auteur and venue and publication :
                self.requestManager.selectVenuePublicationFromKeyword(singleElement)
                return True
            if auteur and not venue and not publication :
                self.requestManager.selectAuteurFromKeyword(singleElement)
                return True
            if  auteur and not venue and publication :
                self.requestManager.selectAuteurPublicationFromKeyword(singleElement)
                return True
            if  auteur and venue and not publication :
                self.requestManager.selectAuteurVenueFromKeyword(singleElement)
                return True
            if  auteur and venue and publication :
                self.requestManager.selectAuteurVenuePublicationFromKeyword(singleElement)
                return True           
        
        return False
        
        
        
    def PreviewRequest(self):
        isValid = self.ExecuteRequest()
        if isValid:
            model = DataFrameModel(self.requestManager.result_df)
            self.ui.PreviewTable.setModel(model)
        
    def ShowGraph(self):
        isValid = self.ExecuteRequest()
        if isValid:
            self.graphWindow.graphManager.GenerateGraph(self.requestManager.result)
            self.graphWindow.show()
        
    def closeEvent(self, event):
        self.graphWindow.close()
        
    def SelectVenues(self):
        dlg = ListDialog(self.listVenues)
        self.listVenues = dlg.GetResults()
    


        

class GraphManager:
    def __init__(self, webview):
        self.webview = webview
        self.graph = Network()
        self.graph.set_options('var options = {"autoResize": true, "height": "100%", "width": "100%", "locale": "fr", "clickToUse": false}')
        
        self.__custom_style__ = """
            body{
                margin: 0;
                padding: 0;
                overflow: hidden;
                background-color: #222222;}
            h1{visibility: collapse;}
            #mynetwork {
                width: 100%;
                height: 100%;
                background-color: #222222;
                border: 0;
                position: absolute;
                top: 0;
                left: 0;}
        """
        
    def GenerateGraph(self, data):
        self.graph = data
        self.graph.barnes_hut(gravity=-40000, central_gravity=0.3, spring_length=250, spring_strength=0.01, damping=0.09, overlap=0)
        self.graph.save_graph("graph.html")
        graph_path = os.path.abspath(os.path.join(mainDir, "graph.html"))
        
        soup = BeautifulSoup(open("graph.html").read(),features="html.parser")
        style_tag = soup.find("style")
        style_tag.string = self.__custom_style__
        open("graph.html", "w", encoding="utf-8").write(str(soup))
        
        self.webview.load(QUrl.fromLocalFile(graph_path))
        

if __name__ == '__main__':
    
    #SETUP APP
    app = QApplication(sys.argv)
    themeFile = "darkorange.qss"
    with open(themeFile,"r") as theme:
        app.setStyleSheet(theme.read())
    
    #SETUP WINDOWS
    graph_win = GraphWindow()
    request_win = RequestWindow(graph_win)
    
    #DISPLAY WINDOWS
    request_win.show()
    
    app.exec_()
    sys.exit()
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    