# -*- coding: utf-8 -*-
import sys
import os
from pyvis.network import Network
from bs4 import BeautifulSoup
from datetime import datetime


from RequestManager import RequestManager
from flask import Flask,render_template,redirect,request,url_for

app = Flask(__name__)
mainDir = os.path.dirname(os.path.realpath(__file__))

requestManager = RequestManager()

custom_style = """
            body{
                margin: 0;
                padding: 0;
                overflow: hidden;
                background-color: #222222;}
            h1{visibility: collapse;}
            #mynetwork {
                width: 100%;
                height: 100%;
                background-color: #222222;
                border: 0;
                position: absolute;
                top: 0;
                left: 0;}
        """


def ExecuteRequest(settings):
        
    singleElement = settings['SingleSelectorInput']
    # if settings['RButtonNameSelect']:
    #     if settings['TableSelector'] > 0:
    #         singleElement = requestManager.getAuteurIdFromName(singleElement)
        
    if settings['LimitResultCheck']:
        requestManager.maxLimit = settings['MaxResultSelector']
    else:
        requestManager.maxLimit = 500
    
    if settings['TableSelector'] == 0:
        
        coAutor = settings['AutorCheck']
        publication = settings['PublicationCheck']
        keyword = settings['KeywordCheck']
        venue = settings['VenueCheck']
        
        if coAutor and not publication and not keyword and not venue:
            requestManager.selectCoAuteurFromAuteur(singleElement)
            return True
        
        if publication and not coAutor and not keyword and not venue:
            requestManager.selectPublicationFromAuteur(singleElement)
            return True
        
        if keyword and not publication and not coAutor and not venue :
            requestManager.selectMotsClesFromAuteur(singleElement)
            return True
        
        if coAutor and publication and not keyword and not venue :
            requestManager. selectCoauteurPublicationFromAuteur(singleElement)
            return True
        
        if coAutor and not publication and keyword and not venue :
            requestManager.selectCoauteurKeywordFromAuteur(singleElement)
            return True
        
        if not coAutor and publication and keyword and not venue:
            requestManager.selectMotsClesAndPublicationsFromAuteur(singleElement)
            return True
        
        if coAutor and publication and keyword and not venue :
            requestManager.selectCoauteurPublicationKeywordFromAuteur(singleElement)
            return True
        
        if not coAutor and not publication and not keyword and not venue:
            return False
         
        if coAutor and not publication and not keyword and  venue:
            requestManager.selectCoauteurVenueFromAuteur(singleElement)
            return True
        
        if publication and not coAutor and not keyword and  venue:
            requestManager.selectPublicationVenueParAuteur(singleElement)
            return True
        
        if keyword and not publication and not coAutor and  venue :
            requestManager.selectKeywordVenueFromAuteur(singleElement)
            return True
        
        if coAutor and publication and not keyword and  venue :
            requestManager.selectCoauteurPublicationVenueFromAuteur(singleElement)
            return True 
        
        if coAutor and not publication and keyword and  venue :
            requestManager.selectCoauteurKeywordVenueFromAuteur(singleElement)
            return True
        if not coAutor and publication and keyword and  venue:
            requestManager.selectPublicationKeywordVenueFromAuteur(singleElement)
            return True
        
        if coAutor and publication and keyword and  venue :
            requestManager.selectgraphfinal(singleElement)
            return True
        
        if not coAutor and not publication and not keyword and venue:
            requestManager.selectVenueParAuteur(singleElement)
            return True           
        
    elif settings['TableSelector'] == 1:
        if settings['RButtonNameSelect'] and len(singleElement) > 0:
            singleElement = requestManager.getPubidFromTitle(singleElement)
        auteur = settings['AutorCheck']
        keyword = settings['KeywordCheck']
        venue = settings['VenueCheck'] 
        year =settings['YearCheck']
        if not auteur and not keyword and not venue and not year :
            return False
        if not auteur and not keyword and  venue and not year :
            requestManager.selectVenueFromPublication(singleElement)
            return True
        if not auteur and  keyword and not venue and not year :
            requestManager.selectKeywordFromPublication(singleElement)
            return True
        if not auteur and  keyword and  venue and not year :
            return False
        if  auteur and not keyword and not venue and not year :
            requestManager.selectAuteurFromPublication(singleElement)
            return True
        if  auteur and not keyword and  venue and not year :
            return False
        if  auteur and  keyword and not venue and not year :
            return False
        if  auteur and  keyword and  venue and not year :
            return False
        
        
        
        if not auteur and not keyword and not venue and  year :
            requestManager.selectYearFromPublication(singleElement)
            return True
        if not auteur and not keyword and  venue and  year :
            return False
        if not auteur and  keyword and not venue and  year :
            return False
        if not auteur and  keyword and  venue and  year :
            return False
        if  auteur and not keyword and not venue and  year :
            return False
        if  auteur and not keyword and  venue and  year :
            return False
        if  auteur and  keyword and not venue and  year :
            return False
        if  auteur and  keyword and  venue and  year :
            return False
        
    elif settings['TableSelector'] == 2:
        if settings['RButtonNameSelect'] and len(singleElement) > 0:
            singleElement = requestManager.getVenueIdFromName(singleElement)
        auteur = settings['AutorCheck']
        keyword = settings['KeywordCheck']
        publication = settings['PublicationCheck']
        if not auteur and not keyword and not publication :
            return False
        if not auteur and not keyword and publication :
            requestManager.selectPublicationFromVenue(singleElement)
            return True
        if not auteur and keyword and not publication :
            requestManager.selectkeywordFromVenue(singleElement)
            return True
        if not auteur and keyword and publication :
            requestManager.selectKeywordPublicationFromVenue(singleElement)
            return True
        if auteur and not keyword and not publication :
            requestManager.selectAuteurFromVenue(singleElement)
            return True
        if  auteur and not keyword and publication :
            requestManager.selectAuteurPublicationFromVenue(singleElement)
            return True
        if  auteur and keyword and not publication :
            requestManager.selectAuteurKeywordFromVenue(singleElement)
            return True
        if  auteur and keyword and publication :
            requestManager.selectAuteurKeywordPublicationFromVenue(singleElement)
            return True
        
        
    elif settings['TableSelector'] == 3:
        auteur = settings['AutorCheck']
        publication = settings['PublicationCheck']
        venue = settings['VenueCheck']
        if not auteur and not venue and not publication :
            return False
        if not auteur and not venue and publication :
            requestManager.selectPublicationFromKeyword(singleElement)
            return True
        if not auteur and venue and not publication :
            requestManager.selectVenueFromKeyword(singleElement)
            return True
        if not auteur and venue and publication :
            requestManager.selectVenuePublicationFromKeyword(singleElement)
            return True
        if auteur and not venue and not publication :
            requestManager.selectAuteurFromKeyword(singleElement)
            return True
        if  auteur and not venue and publication :
            requestManager.selectAuteurPublicationFromKeyword(singleElement)
            return True
        if  auteur and venue and not publication :
            requestManager.selectAuteurVenueFromKeyword(singleElement)
            return True
        if  auteur and venue and publication :
            requestManager.selectAuteurVenuePublicationFromKeyword(singleElement)
            return True           
    
    return False


@app.route('/')
def Index():
    return render_template('index.html')
	
@app.route('/request')
def Request():
    return render_template('request.html')

@app.route('/graph', methods=['POST'])
def Graph():
    settings = {}
    print(request.form)
    settings['TableSelector'] = int(request.form["TableSelect"])
    settings['SingleSelectorInput'] = request.form["SingleSelector"]
    settings['RButtonNameSelect'] = int(request.form.get("NameRadio",0))==1
    
    settings['AutorCheck'] = request.form.get("AutorCheck",0)=='on'
    settings['PublicationCheck'] = request.form.get("PublicationCheck",0)=='on'
    settings['KeywordCheck'] = request.form.get("KeywordCheck",0)=='on'
    settings['VenueCheck'] = request.form.get("VenueCheck",0)=='on'
    
    settings['YearCheck'] = request.form.get("YearCheck",0)=='on'
    
    settings['LimitResultCheck'] = request.form.get("MaxNbCheck",1)=='on'
    settings['MaxResultSelector'] = int(request.form.get("MaxNbInput",100))
    
    
    if ExecuteRequest(settings):
        graph = requestManager.result
        graphName = "graph_"+datetime.now().strftime("%H_%M_%S")+".html"
        graph.save_graph("templates/"+graphName)
        
        soup = BeautifulSoup(open("templates/"+graphName).read(),features="html.parser")
        style_tag = soup.find("style")
        style_tag.string = custom_style
        open("templates/"+graphName, "w", encoding="utf-8").write(str(soup))
        return render_template(graphName)
    else:
        return redirect(url_for('Index'))
    
    return redirect(url_for('Index'))
    