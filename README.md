# Projet Intégré !

# Dataviz 2

# Desciption
Dans la partie Datavisualisation, il s'agira de vous permettre d'établir des recherches et des requêtes sur les différentes variables du jeu de données DPLP qui sont les auteurs, les venues, les publications, les keywords et les années.
Vous devrez entrer l'information principale que vous voulez observer grâce à une liste déroulante et ainsi choisir à l'aide de cases à cocher, les liaisons recherchées. la possibilité de limité le nombre de résultats est ajouté.
Vous pourrez les observer à l'aide de dataframes avec les graphes associés. On pourra déplacer les noeuds si nécessaire, zoomer dessus, et afficher leurs détails en passant la souris par dessus.

# Installation du l'outil

Installation PyQT5 :
ouvrir terminal conda/cmd
pip install pyqt5~=5.15.1
pip install pyqtwebengine~=5.12.1
pip install pyqt5-tools~=5.15
Lancer l’outil de design avec la commande : “pyqt5-tools designer”
Pour convertir un fichier .ui en fichier .py utilisable : pyuic5 [fichier ui] -o [fichier py]
exemple : “pyuic5 .\test.ui -o UI_test.py”

# installation Pandas : https://pandas.pydata.org/pandas-docs/stable/getting_started/install.html
# installation Pyvis : https://pyvis.readthedocs.io/en/latest/install.html
# installation Networkx: https://networkx.org/documentation/stable/install.html

Pour installer l'outil
Lancer le fichier dataviz.py 
Choisir le noeud principale puis cocher les éléments recherchés
Valider la saisie