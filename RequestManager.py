# -*- coding: utf-8 -*-
"""
Created on Wed Nov 25 20:01:14 2020

@author: Evann
"""

import pandas as pd
from pyvis.network import Network
import networkx as nx

class RequestManager:
    def __init__(self):
        self.df_aut= pd.read_csv("data/author.csv",sep=";")
        self.df_pub_Aut= pd.read_csv("data/publication_author.csv",sep=";")
        self.df_ven= pd.read_csv("data/venue.csv",sep=";")
        self.df_key= pd.read_csv("data/keyword.csv",sep=";") 
        self.df_pub_key= pd.read_csv("data/publication_keywords.csv",sep=";")
        self.df_pub_year= pd.read_csv("data/publication_year.csv",sep=";")
        self.df_year= pd.read_csv("data/year.csv",sep=";")
        self.df_pub= pd.read_csv("data/publication.csv",sep=";")
        self.df_pub_venu= pd.read_csv("data/publication_venue.csv",sep=";",error_bad_lines=False)
        
        self.result = Network(height="750px", width="100%", bgcolor="#222222", font_color="white");
        self.result_df = None;
        
        self.maxLimit = 100
   
    # les infos sous forme de string 
    def getInfoAuteur(self,idAuteur):
        df = self.getTreeAuteur(idAuteur)
        if df.empty :
            return "Auteur non trouvé"
        else :
            df = df.iloc[0]
            return "Auteur"+"\n/nom: "+df['author']+"\n/nombre de publication: "+str(df['nbr_publication'])
    
    def getInfoKeyword(self,keyword):
        df = self.df_key.loc[self.df_key.keyword == keyword]
        if df.empty :
            return 
        else :
            df = df.iloc[0]
            return "Mot clé '"+df['keyword']+"'\nutilisé: "+str(df['nbr_utilisation'])
        
    def getInfoVenue(self,idVenu):
        df = self.df_ven.loc[self.df_ven.id_venue == idVenu]
        if df.empty :
            df = self.df_ven.loc[self.df_ven.name_venue == idVenu]
            if not df.empty:
                df = df.iloc[0]
                return "Venue \n id: "+df['id_venue']+"\n nom: "+df['name_venue']
        else :
            df = df.iloc[0]
            return "Venue \n id: "+df['id_venue']+"\n nom: "+df['name_venue']+"\n type: "
    
    def getInfoPublication(self,idPub):
        df = self.df_pub.loc[self.df_pub.id_publication == idPub]
        if df.empty :
            df = self.df_pub.loc[self.df_pub.article_title == idPub]
            if not df.empty :
                df = df.iloc[0]
                return "Publication \n id: "+df['id_publication']+"\n/date publication: "+df['date_pub']+"\n/nombre auteur: "+str(df['nbr_authors']) +"\n titre article: "+df['article_title']+"\n categorie: "+df['categorie']
        else :
            df = df.iloc[0]
            return "Publication \n id: "+df['id_publication']+"\n/date publication: "+df['date_pub']+"\n/nombre auteur: "+str(df['nbr_authors']) +"\n titre article: "+df['article_title']+"\n categorie: "+df['categorie']
    
    def getAuteurIdFromName(self, nameAuteur):
        foundAutor = self.df_aut.loc[self.df_aut.author == nameAuteur].head(1)
        return foundAutor['author'].iloc[0]
    def getVenueIdFromName(self, nameVenue):
        foundAutor = self.df_ven.loc[self.df_ven.name_venue == nameVenue].head(1)
        return foundAutor['id_venue'].iloc[0]
    
    def getTitle_from_pub(self,idpub):
        df = self.df_pub.loc[self.df_pub.id_publication == idpub]
        if df.empty :
            return
        else :
            return str(df.iloc[0]["article_title"])
    def getPubidFromTitle(self , title):
        foundAutor = self.df_pub.loc[self.df_pub.article_title == title].head(1)
        return foundAutor['id_publication'].iloc[0]        
        
      
    def getTreeKeyword(self,keyword):
        dfTree = pd.merge(self.df_key.loc[self.df_key.keyword==keyword],  self.df_pub_key, left_on='keyword',          right_on='keyword',             how='left')
        dfTree = pd.merge(dfTree,                               self.df_pub,     left_on='id_publication',   right_on='id_publication',      how='left')
        dfTree = pd.merge(dfTree,                               self.df_pub_Aut, left_on='id_publication',   right_on='id_publication',      how='left')
        dfTree = pd.merge(dfTree,                               self.df_pub_venu,left_on='id_publication',   right_on='id_publication',      how='left')
        dfTree = pd.merge(dfTree,                               self.df_pub_year,left_on='id_publication',   right_on='id_publication',      how='left')
        dfTree = pd.merge(dfTree,                               self.df_aut,     left_on='author',        right_on='author',           how='left')    
        dfTree = pd.merge(dfTree,                               self.df_ven,     left_on='id_venue',         right_on='id_venue',            how='left')
        return dfTree

    # récupérer toute l'arborescence d'une publication    
    def getTreePublication(self,idPublication):
        dfTree = pd.merge(self.df_pub[self.df_pub.id_publication==idPublication], self.df_pub_key, left_on='id_publication',   right_on='id_publication',      how='left')
        dfTree = pd.merge(dfTree,                                       self.df_pub_Aut, left_on='id_publication',   right_on='id_publication',      how='left')
        dfTree = pd.merge(dfTree,                                       self.df_pub_venu,left_on='id_publication',   right_on='id_publication',      how='left')
        dfTree = pd.merge(dfTree,                                       self.df_pub_year,left_on='id_publication',   right_on='id_publication',      how='left')
        dfTree = pd.merge(dfTree,                                       self.df_aut,     left_on='author',        right_on='author',           how='left')    
        dfTree = pd.merge(dfTree,                                       self.df_ven,     left_on='id_venue',         right_on='id_venue',            how='left')
        dfTree = pd.merge(dfTree,                                       self.df_key,     left_on='keyword',          right_on='keyword',             how='left')
        return dfTree
    
    def getCoAuteurFromAuteur(self,idAuteur):
        #selectionner les publications de l'auteur rechrché 
        df_pub_auteur = self.df_pub_Aut.loc[self.df_pub_Aut.author == idAuteur]
        #selectionner les autres auteurs ayant les publciation de l'auteur recherché
        is_co_auteur = self.df_pub_Aut.id_publication.isin(df_pub_auteur.id_publication)
        # jointure entre le deux tables pour avoir les noms des auteurs 
        df_co_auteur=pd.merge(self.df_pub_Aut[is_co_auteur],self.df_aut, left_on='author', right_on='author',suffixes=('_inner', '_right'), how='inner')
        df_co_auteur = df_co_auteur[['author']].drop_duplicates()
        df_co_auteur = df_co_auteur.loc[df_co_auteur.author!=idAuteur]
        #df_co_auteur = df_co_auteur.rename(index={0: "id_co_author", 1: "name_author", 2: "author"})
        df_co_auteur = df_co_auteur.rename(columns={"author": "name_co_author"})
        df_co_auteur = df_co_auteur.assign(author=idAuteur)
        return df_co_auteur
    

    
    # récupérer toute l'arborescence d'une venue
    def getTreeVenue(self,idVenue):
        dfTree = pd.merge(self.df_ven[self.df_ven.id_venue==idVenue],   self.df_pub_venu, left_on='id_venue',         right_on='id_venue',           how='left')
        dfTree = pd.merge(dfTree,                                       self.df_pub_Aut,  left_on='id_publication',   right_on='id_publication',     how='left')
        dfTree = pd.merge(dfTree,                                       self.df_pub_year, left_on='id_publication',   right_on='id_publication',     how='left') 
        dfTree = pd.merge(dfTree,                                       self.df_pub_key,  left_on='id_publication',   right_on='id_publication',     how='left')    
        dfTree = pd.merge(dfTree,                                       self.df_aut,      left_on='author',        right_on='author',          how='left')
        dfTree = pd.merge(dfTree,                                       self.df_pub,      left_on='id_publication',   right_on='id_publication',     how='left')
        dfTree = pd.merge(dfTree,                                       self.df_key,      left_on='keyword',          right_on='keyword',            how='left')
        return dfTree
    def getTreeAuteur(self,idAuteur):
        dfTree = pd.merge(self.df_aut[self.df_aut.author==idAuteur],           self.df_pub_Aut, left_on='author',        right_on='author',           how='left')
        dfTree = pd.merge(dfTree,                                       self.df_pub_venu,left_on='id_publication',   right_on='id_publication',      how='left')
        dfTree = pd.merge(dfTree,                                       self.df_pub_year,left_on='id_publication',   right_on='id_publication',      how='left')
        dfTree = pd.merge(dfTree,                                       self.df_pub_key, left_on='id_publication',   right_on='id_publication',      how='left')
        dfTree = pd.merge(dfTree,                                       self.df_ven,     left_on='id_venue',         right_on='id_venue',            how='left')
        dfTree = pd.merge(dfTree,                                       self.df_pub,     left_on='id_publication',   right_on='id_publication',      how='left')
        dfTree = pd.merge(dfTree,                                       self.df_key,     left_on='keyword',          right_on='keyword',             how='left')
        df_co_authors = self.getCoAuteurFromAuteur(idAuteur)
        dfTree = pd.merge(dfTree,           df_co_authors,     left_on='author',          right_on='author',           how='left')

        return dfTree 
    
    def selectYearFromPublication(self,id_Pub):
        df = self.getTreePublication(id_Pub)
        if df.empty :
            self.result_df = pd.merge(self.df_pub,self.df_pub_year,on = 'id_publication')[['article_title','id_year']]
 
            G = nx.from_pandas_edgelist(self.result_df,source="article_title",target="id_year")
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(G)
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes :
                if not self.result_df.loc[self.result_df.article_title == node["label"]].empty :
                    node["color"] = "green"
                    node["shape"] = "star"
                    node["title"] = self.getInfoPublication(node["label"])                    
                else:
                    node["title"] = node["label"]
                    node["size"] = 5 * len(voisin[node["id"]])
            self.result = G_Dyn                                        
            return
        
        else :
            self.result_df =df[['article_title','id_year']]
            G = nx.from_pandas_edgelist(self.result_df,source="article_title",target="id_year")
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(G)
            for node in G_Dyn.nodes :
                if not self.result_df.loc[self.result_df.article_title == node["label"]].empty :
                    node["color"] = "green"
                    node["shape"] = "star"
                    node["title"] = self.getInfoPublication(node["label"])                    
                else:
                    node["title"] = node["label"]
            self.result = G_Dyn 
            return
    
    def selectKeywordFromPublication(self,id_Pub):
        print('were here')
        df = self.getTreePublication(id_Pub)
        if df.empty :
            self.result_df = pd.merge(self.df_pub,self.df_pub_key,on = 'id_publication')[['article_title','keyword']]
 
            G = nx.from_pandas_edgelist(self.result_df,source="article_title",target="keyword")
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(G)
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes :
                if not self.result_df.loc[self.result_df.article_title == node["label"]].empty :
                    node["color"] = "green"
                    node["shape"] = "star"
                    node["size"] = 5 * len(voisin[node["id"]])
                    node["title"] = self.getInfoPublication(node["label"])                    
                else:
                    node["title"] = self.getInfoKeyword(node["label"])
            self.result = G_Dyn                                        
            return
        
        else :
            self.result_df =df[['article_title','keyword']]
            G = nx.from_pandas_edgelist(self.result_df,source="article_title",target="keyword")
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(G)
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes :
                if not self.result_df.loc[self.result_df.article_title == node["label"]].empty :
                    node["color"] = "green"
                    node["shape"] = "star"
                    node["size"] = 5 * len(voisin[node["id"]])
                    node["title"] = self.getInfoPublication(node["label"])                    
                else:
                    node["title"] = self.getInfoKeyword(node["label"])
            self.result = G_Dyn 
            return
    
    def selectAuteurFromPublication(self,id_Pub):
        df = self.getTreePublication(id_Pub)
        if df.empty :
            self.result_df = pd.merge(self.df_pub,self.df_pub_Aut,on = 'id_publication')[['article_title','author']]
 
            G = nx.from_pandas_edgelist(self.result_df,source="article_title",target="author")
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(G)
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes :
                if not self.result_df.loc[self.result_df.article_title == node["label"]].empty :
                    node["color"] = "green"
                    node["shape"] = "star"
                    node["size"] = 5 * len(voisin[node["id"]])
                    node["title"] = self.getInfoPublication(node["label"])                    
                else:
                    node["title"] = self.getInfoAuteur(node["label"])
            self.result = G_Dyn                                        
            return
        
        else :
            self.result_df =df[['article_title','author']]
            G = nx.from_pandas_edgelist(self.result_df,source="article_title",target="author")
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(G)
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes :
                if not self.result_df.loc[self.result_df.article_title == node["label"]].empty :
                    node["color"] = "green"
                    node["shape"] = "star"
                    node["size"] = 5 * len(voisin[node["id"]])
                    node["title"] = self.getInfoPublication(node["label"])                    
                else:
                    node["title"] = self.getInfoAuteur(node["label"])
            self.result = G_Dyn 
            return
    
    def selectVenueFromPublication(self,id_Pub):
        df = self.getTreePublication(id_Pub)
        if df.empty :
            self.result_df = pd.merge(self.df_pub,self.df_pub_venu,on = 'id_publication').merge(self.df_ven,on='id_venue')[['article_title','name_venue']]
 
            G = nx.from_pandas_edgelist(self.result_df,source="article_title",target="name_venue")
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(G)
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes :
                if not self.result_df.loc[self.result_df.article_title == node["label"]].empty :
                    node["color"] = "green"
                    node["shape"] = "star"
                    node["size"] = 5 * len(voisin[node["id"]])
                    node["title"] = self.getInfoPublication(node["label"])                    
                else:
                    node["title"] = self.getInfoVenue(node["label"])
            self.result = G_Dyn                                        
            return
        
        else :
            self.result_df =df[['article_title','name_venue']]
            G = nx.from_pandas_edgelist(self.result_df,source="article_title",target="name_venue")
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(G)
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes :
                if not self.result_df.loc[self.result_df.article_title == node["label"]].empty :
                    node["color"] = "green"
                    node["shape"] = "star"
                    node["size"] = 5 * len(voisin[node["id"]])
                    node["title"] = self.getInfoPublication(node["label"])                    
                else:
                    node["title"] = self.getInfoVenue(node["label"])
            self.result = G_Dyn 
            return
         

    def selectPublicationVenueParAuteur(self,idAuteur):
        df = self.getTreeAuteur(idAuteur)
        if df.empty:
            self.result_df = pd.merge(self.df_pub_Aut,self.df_pub_venu,on="id_publication").merge(self.df_ven,on="id_venue").merge(self.df_pub,on="id_publication")[['author','article_title','name_venue']]
            G = nx.from_pandas_edgelist(self.result_df,source="name_venue",target="article_title")
            H = nx.from_pandas_edgelist(self.result_df,source="name_venue",target="author") 
            F = nx.from_pandas_edgelist(self.result_df,source="author",target="article_title")
            graphfinal = nx.compose(G,H)
            graphfinal = nx.compose(graphfinal,F)
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal)
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes :
                if not self.df_pub.loc[self.df_pub.article_title == node["label"]].empty :
                    node["color"] = "orange"
                    node["size"] = 20
                    node["title"] = self.getInfoPublication(node["label"]) 
                elif not self.df_aut.loc[self.df_aut.author == node["label"]].empty :
                    node["color"] = "green"
                    node["shape"] = "star"
                    node["size"] = 5 * len(voisin[node["id"]])
                    node["title"] = self.getInfoAuteur(node["label"])
                else:
                    node["color"] = "red"
                    node["size"] = 30
                    node["title"] = self.getInfoVenue(node["label"])
           
            self.result = G_Dyn
            return
        else : 
            return
    def selectKeywordVenueFromAuteur(self,idAuteur):
        df = self.getTreeAuteur(idAuteur)
        if df.empty :
            self.result_df = pd.merge(self.df_pub_key,self.df_pub_venu,on="id_publication").merge(self.df_pub_Aut,on="id_publication").merge(self.df_ven,on="id_venue")[['author','keyword','name_venue']]
            G = nx.from_pandas_edgelist(self.result_df,source="author",target="name_venue")
            H = nx.from_pandas_edgelist(self.result_df,source="author",target="keyword")
            F = nx.from_pandas_edgelist(self.result_df,source="keyword",target="name_venue")
            graphfinal = nx.compose(G,H)
            graphfinal = nx.compose(graphfinal,F)
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal)
            voisin = G_Dyn.get_adj_list()            
            for node in G_Dyn.nodes : 
                if not self.df_aut.loc[self.df_aut.author == node["label"]].empty : 
                    node["color"] = "green"
                    node["shape"] = "star"
                    node["size"] = 5 * len(voisin[node["id"]])
                    node["title"] = self.getInfoAuteur(node["label"]) 
                elif not self.df_key.loc[self.df_key.keyword == node["label"]].empty :
                    node["color"] = "blue"
                    node["size"] = 20
                    node["title"] = self.getInfoKeyword(node["label"])
                else :
                    node["color"] = "red"
                    node["size"] = 30
                    node["title"] = self.getInfoVenue(node["label"]) 
            self.result = G_Dyn
            return
        else :
            self.result_df = df[['author','keyword','name_venue']]
            G = nx.from_pandas_edgelist(self.result_df,source="author",target="name_venue")
            H = nx.from_pandas_edgelist(self.result_df,source="author",target="keyword")
            F = nx.from_pandas_edgelist(self.result_df,source="keyword",target="name_venue")
            graphfinal = nx.compose(G,H)
            graphfinal = nx.compose(graphfinal,F)
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal)
            voisin = G_Dyn.get_adj_list()            
            for node in G_Dyn.nodes : 
                if not self.df_aut.loc[self.df_aut.author == node["label"]].empty : 
                    node["color"] = "green"
                    node["shape"] = "star"
                    node["size"] = 5 * len(voisin[node["id"]])
                    node["title"] = self.getInfoAuteur(node["label"]) 
                elif not self.df_key.loc[self.df_key.keyword == node["label"]].empty :
                    node["color"] = "blue"
                    node["size"] = 20
                    node["title"] = self.getInfoKeyword(node["label"])
                else :
                    node["color"] = "red"
                    node["size"] = 30
                    node["title"] = self.getInfoVenue(node["label"]) 
            self.result = G_Dyn       
            return            
    def selectCoauteurPublicationVenueFromAuteur(self,idAuteur)  :
        df = self.getTreeAuteur(idAuteur)
        if df.empty :
            self.result_df = pd.merge(self.df_pub_Aut,self.df_pub_Aut,on="id_publication")
            self.result_df = pd.merge(self.result_df,self.df_pub_venu,on="id_publication")
            self.result_df = pd.merge(self.result_df,self.df_ven,on="id_venue")
            self.result_df = self.result_df.rename(columns = {'author_x' : 'author','author_y' : 'co_auteur'})
            self.result_df = self.result_df.loc[self.result_df.author != self.result_df.co_auteur]
            self.result_df = self.result_df[['author','co_auteur','name_venue','id_publication']].merge(self.df_pub,on = 'id_publication')[['author','co_auteur','name_venue','article_title']]
            G = nx.from_pandas_edgelist(self.result_df,source = 'author', target= 'co_auteur')
            H = nx.from_pandas_edgelist(self.result_df,source = 'author', target= 'name_venue')
            F =  nx.from_pandas_edgelist(self.result_df,source = 'author', target= 'article_title')
            C =  nx.from_pandas_edgelist(self.result_df,source = 'co_auteur', target= 'name_venue')
            D =  nx.from_pandas_edgelist(self.result_df,source = 'co_auteur', target= 'article_title')
            E =  nx.from_pandas_edgelist(self.result_df,source = 'article_title', target= 'name_venue')
            graphfinal = nx.compose(G,H)
            graphfinal = nx.compose(graphfinal,F)
            graphfinal = nx.compose(graphfinal,C)
            graphfinal = nx.compose(graphfinal,D)
            graphfinal = nx.compose(graphfinal,E)
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal) 
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes : 
                if not self.result_df.loc[self.result_df.name_venue == node["label"]].empty : 
                    node["color"] = "yellow"
                    node["size"] =20 
                    node["title"] = self.getInfoVenue(node["label"])                   
                elif not self.result_df.loc[self.result_df.author == node["label"]].empty :
                    node["color"] = "green"
                    node["shape"] = "star"
                    node["size"] = 5 * len(voisin[node["id"]])
                    node["title"] = self.getInfoAuteur(node["label"])                    
                else:
                    node["color"] = "red"
                    node["size"] = 30
                    node["title"] = self.getInfoPublication(node["label"])  
            self.result = G_Dyn
            return
        else :
            self.result_df = df[['author','article_title','name_co_author','name_venue']]
            G = nx.from_pandas_edgelist(self.result_df,source = 'author', target= 'name_co_author')
            H = nx.from_pandas_edgelist(self.result_df,source = 'author', target= 'name_venue')
            F =  nx.from_pandas_edgelist(self.result_df,source = 'author', target= 'article_title')
            C =  nx.from_pandas_edgelist(self.result_df,source = 'name_co_author', target= 'name_venue')
            D =  nx.from_pandas_edgelist(self.result_df,source = 'name_co_author', target= 'article_title')
            E =  nx.from_pandas_edgelist(self.result_df,source = 'article_title', target= 'name_venue')
            graphfinal = nx.compose(G,H)
            graphfinal = nx.compose(graphfinal,F)
            graphfinal = nx.compose(graphfinal,C)
            graphfinal = nx.compose(graphfinal,D)
            graphfinal = nx.compose(graphfinal,E)
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal) 
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes : 
                if not self.result_df.loc[self.result_df.name_venue == node["label"]].empty : 
                    node["color"] = "yellow"
                    node["size"] =20 
                    node["title"] = self.getInfoVenue(node["label"])                   
                elif not self.result_df.loc[self.result_df.author == node["label"] ].empty  or  not self.result_df.loc[self.result_df.name_co_author == node["label"]].empty:
                    if idAuteur in node["label"] : node["color"] = "green"
                    node["shape"] = "star"
                    node["size"] = 5 * len(voisin[node["id"]])
                    node["title"] = self.getInfoAuteur(node["label"])                    
                else:
                    node["color"] = "red"
                    node["size"] = 30
                    node["title"] = self.getInfoPublication(node["label"])  
            self.result = G_Dyn
            return
        
        return  
    def selectCoauteurKeywordVenueFromAuteur(self,idAuteur):
        df = self.getTreeAuteur(idAuteur)
        if df.empty :
            self.result_df = pd.merge(self.df_pub_Aut,self.df_pub_Aut,on="id_publication")
            self.result_df = pd.merge(self.result_df,self.df_pub_venu,on="id_publication")
            self.result_df = pd.merge(self.result_df,self.df_ven,on="id_venue")
            self.result_df = self.result_df.rename(columns = {'author_x' : 'author','author_y' : 'co_auteur'})
            self.result_df = self.result_df.loc[self.result_df.author != self.result_df.co_auteur]
            self.result_df = self.result_df[['author','co_auteur','name_venue','id_publication']].merge(self.df_pub_key,on = 'id_publication')[['author','co_auteur','name_venue','keyword']]
            G = nx.from_pandas_edgelist(self.result_df,source = 'author', target= 'co_auteur')
            H = nx.from_pandas_edgelist(self.result_df,source = 'author', target= 'name_venue')
            F =  nx.from_pandas_edgelist(self.result_df,source = 'author', target= 'keyword')
            C =  nx.from_pandas_edgelist(self.result_df,source = 'co_auteur', target= 'name_venue')
            D =  nx.from_pandas_edgelist(self.result_df,source = 'co_auteur', target= 'keyword')
            E =  nx.from_pandas_edgelist(self.result_df,source = 'keyword', target= 'name_venue')
            graphfinal = nx.compose(G,H)
            graphfinal = nx.compose(graphfinal,F)
            graphfinal = nx.compose(graphfinal,C)
            graphfinal = nx.compose(graphfinal,D)
            graphfinal = nx.compose(graphfinal,E)
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal) 
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes : 
                if not self.result_df.loc[self.result_df.name_venue == node["label"]].empty : 
                    node["color"] = "yellow"
                    node["size"] =20 
                    node["title"] = self.getInfoVenue(node["label"])                   
                elif not self.result_df.loc[self.result_df.author == node["label"]].empty :
                    node["color"] = "green"
                    node["shape"] = "star"
                    node["size"] = 5 * len(voisin[node["id"]])
                    node["title"] = self.getInfoAuteur(node["label"])                    
                else:
                    node["color"] = "blue"
                    node["size"] = 10
                    node["title"] = self.getInfoKeyword(node["label"])  
            self.result = G_Dyn
            return
        else :
            self.result_df = df[['author','name_co_author','name_venue','keyword']]
            G = nx.from_pandas_edgelist(self.result_df,source = 'author', target= 'name_co_author')
            H = nx.from_pandas_edgelist(self.result_df,source = 'author', target= 'name_venue')
            F =  nx.from_pandas_edgelist(self.result_df,source = 'author', target= 'keyword')
            C =  nx.from_pandas_edgelist(self.result_df,source = 'name_co_author', target= 'name_venue')
            D =  nx.from_pandas_edgelist(self.result_df,source = 'name_co_author', target= 'keyword')
            E =  nx.from_pandas_edgelist(self.result_df,source = 'keyword', target= 'name_venue')
            graphfinal = nx.compose(G,H)
            graphfinal = nx.compose(graphfinal,F)
            graphfinal = nx.compose(graphfinal,C)
            graphfinal = nx.compose(graphfinal,D)
            graphfinal = nx.compose(graphfinal,E)
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal) 
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes : 
                if not self.result_df.loc[self.result_df.name_venue == node["label"]].empty : 
                    node["color"] = "yellow"
                    node["size"] =20 
                    node["title"] = self.getInfoVenue(node["label"])                   
                elif not self.result_df.loc[self.result_df.author == node["label"]].empty or  not self.result_df.loc[self.result_df.name_co_author == node["label"]].empty:
                    if idAuteur in node["label"] :
                         node["color"] = "green"
                       
                    node["shape"] = "star"
                    node["size"] = 5 * len(voisin[node["id"]])
                    node["title"] = self.getInfoAuteur(node["label"])                    
                else:
                    node["color"] = "blue"
                    node["size"] = 10
                    node["title"] = self.getInfoKeyword(node["label"])  
            self.result = G_Dyn            
            return
    def selectAuteurFromVenue(self,idVenue):
        df = self.getTreeVenue(idVenue)
        if df.empty:
            self.result_df = pd.merge(self.df_pub_Aut,self.df_pub_venu,on='id_publication').merge(self.df_ven,on = 'id_venue')[['author','name_venue']]
            G = nx.from_pandas_edgelist(self.result_df,source = 'name_venue',target = 'author') 
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(G) 
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes :
                if not self.result_df.loc[self.result_df.author == node["label"]].empty :
                    node["color"] = 'red'
                    node["size"] = 20
                    node["title"] = self.getInfoAuteur(node["label"])                     
                    
                else:
                    node["shape"] = "star"
                    node["size"] = 5 * len(voisin[node["id"]])
                    node["title"] = self.getInfoVenue(node["label"]) 
            self.result = G_Dyn
            return
        else :
            self.result_df =df[['author','name_venue']]
            G = nx.from_pandas_edgelist(self.result_df,source = 'name_venue',target = 'author') 
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(G) 
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes :
                if not self.result_df.loc[self.result_df.author == node["label"]].empty :
                    node["size"] = 20
                    node["title"] = self.getInfoAuteur(node["label"])                     
                    
                else:
                    node["shape"] = "star"
                    node ["color"] = "yellow"
                    node["size"] = 5 * len(voisin[node["id"]])
                    node["title"] = self.getInfoVenue(node["label"]) 
            self.result = G_Dyn
            return
    def selectAuteurKeywordFromVenue(self,idVenue) :
         df = self.getTreeVenue(idVenue)
         if  df.empty :
            self.result_df = self.df_pub_venu.merge(self.df_pub_Aut,on="id_publication")
            self.result_df = self.result_df.merge(self.df_pub_key,on="id_publication").merge(self.df_ven,on='id_venue')
            self.result_df = self.result_df[['name_venue','author','keyword']]
            G = nx.from_pandas_edgelist(self.result_df,source="name_venue",target="author")
            H= nx.from_pandas_edgelist(self.result_df,source="name_venue",target="keyword")
            E=nx.from_pandas_edgelist(self.result_df,source="keyword",target="author")
            graphfinal=nx.compose(G,H)
            graphfinal=nx.compose(graphfinal,E)
    
    
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal)
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes :
  
                if not self.result_df[self.result_df.name_venue == node["label"]].empty:
                    node["color"] = "yellow"
                    node["shape"] = "star"
                    node ["size"] = len(voisin[node["id"]])*5
                    node["title"] = self.getInfoVenue(node["label"])
    
                elif not self.result_df.loc[self.result_df.author == node["label"]].empty:
                    node["title"] =  self.getInfoAuteur(node["label"])
                    node["color"] = "red"
                    node["size"] = 30
    
                else :
                    node["color"] = "blue"
                    node["title"] = self.getInfoKeyword(node["label"])
            self.result = G_Dyn
            return
         else :
            self.result_df = df [['name_venue','author','keyword']]
            C = nx.from_pandas_edgelist(self.result_df,source="keyword",target="name_venue")
            F = nx.from_pandas_edgelist(self.result_df,source="name_venue",target="author")
            G = nx.from_pandas_edgelist(self.result_df,source="keyword",target="author")
            graphfinal = nx.compose(F,G)
            graphfinal = nx.compose(graphfinal,C)
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal)
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes :
    
                if not self.result_df.loc[self.result_df.name_venue == node["label"]].empty:
    
    
                    node["color"] = "yellow"
                    node["shape"] = "star"
                    node ["size"] = len(voisin[node["id"]])*5
                    node["title"] = self.getInfoVenue(node["label"])
    
                elif not self.result_df.loc[self.result_df.author == node["label"]].empty:
                    node["title"] =  self.getInfoAuteur(node["label"])
                    node["color"] = "red"
                    node["size"] = 20
    
    
                else :
                    node["color"] = "blue"
                    node["title"] = self.getInfoKeyword(node["label"])
            self.result = G_Dyn
            return         
        
         return       
    def selectAuteurKeywordPublicationFromVenue(self,idVenue):
        df = self.getTreeVenue(idVenue)
        if df.empty :
            self.result_df = pd.merge(self.df_pub_Aut,self.df_pub_venu,on="id_publication").merge(self.df_pub_key,on="id_publication")
            self.result_df = pd.merge(self.result_df,self.df_pub,on='id_publication').merge(self.df_ven,on ='id_venue')
            self.result_df = self.result_df[['name_venue','article_title','author','keyword']]
            G = nx.from_pandas_edgelist(self.result_df,source="article_title",target= "name_venue")
            H = nx.from_pandas_edgelist(self.result_df,source="article_title",target="keyword")
            F = nx.from_pandas_edgelist(self.result_df,source="name_venue",target="keyword")
            C = nx.from_pandas_edgelist(self.result_df,source="name_venue",target="author")
            D = nx.from_pandas_edgelist(self.result_df,source="keyword",target="author")
            E = nx.from_pandas_edgelist(self.result_df,source="article_title",target="author")
            graphfinal = nx.compose(G,H)
            graphfinal = nx.compose(graphfinal,F)
            graphfinal = nx.compose(graphfinal,C)
            graphfinal = nx.compose(graphfinal,D)
            graphfinal = nx.compose(graphfinal,E)
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal)
            voisin = G_Dyn.get_adj_list();
            for node in G_Dyn.nodes :
                if not self.result_df.loc[self.result_df.article_title == node['label']].empty : 
                    node["title"] = self.getInfoPublication(node["label"])
                    node["color"] = "#dd4b39"

                elif not self.result_df.loc[self.result_df.name_venue == node["label"]].empty:
                    node["title"] = self.getInfoVenue(node["label"])
                    node["size"] =10*len(voisin[node["id"]])
                    node["shape"] = "star"
                    node["color"] = "yellow"
                elif not self.result_df.loc[self.result_df.keyword == node["label"]].empty: 
                    node["title"] = self.getInfoKeyword(node["label"])
                    node["color"] = "blue"
                else:
                    node["color" ] = "red"
                    node["size"] = 20
                    node["title"] = self.getInfoAuteur(node["label"])
                    
            self.result = G_Dyn
            return 
        else :
            self.result_df = df[['name_venue','article_title','author','keyword']]
            G = nx.from_pandas_edgelist(self.result_df,source="article_title",target= "name_venue")
            H = nx.from_pandas_edgelist(self.result_df,source="article_title",target="keyword")
            F = nx.from_pandas_edgelist(self.result_df,source="name_venue",target="keyword")
            C = nx.from_pandas_edgelist(self.result_df,source="name_venue",target="author")
            D = nx.from_pandas_edgelist(self.result_df,source="keyword",target="author")
            E = nx.from_pandas_edgelist(self.result_df,source="article_title",target="author")
            graphfinal = nx.compose(G,H)
            graphfinal = nx.compose(graphfinal,F)
            graphfinal = nx.compose(graphfinal,C)
            graphfinal = nx.compose(graphfinal,D)
            graphfinal = nx.compose(graphfinal,E)
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal)
            voisin = G_Dyn.get_adj_list();
            for node in G_Dyn.nodes :
                if not self.result_df.loc[self.result_df.article_title == node['label']].empty : 
                    node["title"] = self.getInfoPublication(node["label"])
                    node["color"] = "#dd4b39"

                elif not self.result_df.loc[self.result_df.name_venue == node["label"]].empty:
                    node["title"] = self.getInfoVenue(node["label"])
                    node["size"] =10*len(voisin[node["id"]])
                    node["shape"] = "star"
                    node["color"] = "yellow"
                elif not self.result_df.loc[self.result_df.keyword == node["label"]].empty: 
                    node["title"] = self.getInfoKeyword(node["label"])
                    node["color"] = "blue"
                else:
                    node["color" ] = "red"
                    node["size"] = 20
                    node["title"] = self.getInfoAuteur(node["label"])
                    
            self.result = G_Dyn
            return 
        
    def selectgraphfinal(self,idAuteur):
        df = self.getTreeAuteur(idAuteur)
        if df.empty :
            self.result_df = pd.merge(self.df_pub_Aut,self.df_pub_venu,on="id_publication").merge(self.df_pub_key,on="id_publication")
            self.result_df = pd.merge(self.result_df,self.df_pub,on='id_publication').merge(self.df_ven,on ='id_venue')
            self.result_df = self.result_df[['author','article_title','name_venue','keyword']]
            G = nx.from_pandas_edgelist(self.result_df,source="article_title",target= "name_venue")
            H = nx.from_pandas_edgelist(self.result_df,source="article_title",target="keyword")
            F = nx.from_pandas_edgelist(self.result_df,source="name_venue",target="keyword")
            C = nx.from_pandas_edgelist(self.result_df,source="name_venue",target="author")
            D = nx.from_pandas_edgelist(self.result_df,source="keyword",target="author")
            E = nx.from_pandas_edgelist(self.result_df,source="article_title",target="author")
            graphfinal = nx.compose(G,H)
            graphfinal = nx.compose(graphfinal,F)
            graphfinal = nx.compose(graphfinal,C)
            graphfinal = nx.compose(graphfinal,D)
            graphfinal = nx.compose(graphfinal,E)
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal)
            voisin = G_Dyn.get_adj_list();
            for node in G_Dyn.nodes :
                if not self.result_df.loc[self.result_df.article_title == node['label']].empty : 
                    node["title"] = self.getInfoPublication(node["label"])
                    node["color"] = "purple"

                elif not self.result_df.loc[self.result_df.author == node["label"]].empty:
                    node["title"] = self.getInfoAuteur(node["label"])
                    node["size"] =10*len(voisin[node["id"]])
                    node["shape"] = "star"
                    node["color"] = "green"
    
                elif not self.result_df.loc[self.result_df.name_venue == node["label"]].empty: 
                    node["title"] = self.getInfoVenue(node["label"])
                    node["color"] = "blue"
                else:
                    node["color" ] = "red"
                    node["size"] = 20
                    node["title"] = self.getInfoKeyword(node["label"])
                    
            self.result = G_Dyn
            return 
        else :
            self.result_df = df[['author','article_title','name_venue','keyword']]
            G = nx.from_pandas_edgelist(self.result_df,source="article_title",target= "name_venue")
            H = nx.from_pandas_edgelist(self.result_df,source="article_title",target="keyword")
            F = nx.from_pandas_edgelist(self.result_df,source="name_venue",target="keyword")
            C = nx.from_pandas_edgelist(self.result_df,source="name_venue",target="author")
            D = nx.from_pandas_edgelist(self.result_df,source="keyword",target="author")
            E = nx.from_pandas_edgelist(self.result_df,source="article_title",target="author")
            graphfinal = nx.compose(G,H)
            graphfinal = nx.compose(graphfinal,F)
            graphfinal = nx.compose(graphfinal,C)
            graphfinal = nx.compose(graphfinal,D)
            graphfinal = nx.compose(graphfinal,E)
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal)
            voisin = G_Dyn.get_adj_list();
            for node in G_Dyn.nodes :
                if not self.result_df.loc[self.result_df.article_title == node['label']].empty : 
                    node["title"] = self.getInfoPublication(node["label"])
                    node["color"] = "green"

                elif not self.result_df.loc[self.result_df.author == node["label"]].empty:
                    node["title"] = self.getInfoAuteur(node["label"])
                    node["size"] =10*len(voisin[node["id"]])
                    node["shape"] = "star"

                elif not self.result_df.loc[self.result_df.name_venue == node["label"]].empty: 
                    node["title"] = self.getInfoVenue(node["label"])
                    node["color"] = "blue"
                else:
                    node["color" ] = "red"
                    node["size"] = 20
                    node["title"] = self.getInfoKeyword(node["label"])
                    
            self.result = G_Dyn
            return 
    
    def selectAuteurPublicationFromVenue(self,idVenue):
        df = self.getTreeVenue(idVenue)
        if  df.empty :
            self.result_df = self.df_pub_venu.merge(self.df_pub_Aut,on="id_publication")
            self.result_df = self.result_df.merge(self.df_pub,on="id_publication").merge(self.df_ven,on='id_venue')
            self.result_df = self.result_df[['name_venue','article_title','author']]
            G = nx.from_pandas_edgelist(self.result_df,source="name_venue",target="author")
            H= nx.from_pandas_edgelist(self.result_df,source="name_venue",target="article_title")
            E=nx.from_pandas_edgelist(self.result_df,source="article_title",target="author")
            graphfinal=nx.compose(G,H)
            graphfinal=nx.compose(graphfinal,E)
    
    
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal)
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes :
  
                if not self.result_df[self.result_df.name_venue == node["label"]].empty:
                    node["color"] = "yellow"
                    node["shape"] = "star"
                    node ["size"] = len(voisin[node["id"]])*5
                    node["title"] = self.getInfoVenue(node["label"])
    
                elif not self.result_df.loc[self.result_df.author == node["label"]].empty:
                    node["title"] =  self.getInfoAuteur(node["label"])
                    node["color"] = "red"
    
    
                else :
                    node["color"] = "orange"
                    node["shape"] = "star"
                    node ["size"] =  30
                    node["title"] = self.getInfoPublication(node["label"])
            self.result = G_Dyn
            return
        else :
            self.result_df = df [['name_venue','article_title','author']]
            C = nx.from_pandas_edgelist(self.result_df,source="article_title",target="name_venue")
            F = nx.from_pandas_edgelist(self.result_df,source="name_venue",target="author")
            G = nx.from_pandas_edgelist(self.result_df,source="article_title",target="author")
            graphfinal = nx.compose(F,G)
            graphfinal = nx.compose(graphfinal,C)
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal)
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes :
    
                if not self.result_df.loc[self.result_df.name_venue == node["label"]].empty:
    
    
                    node["color"] = "yellow"
                    node["shape"] = "star"
                    node ["size"] = len(voisin[node["id"]])*5
                    node["title"] = self.getInfoVenue(node["label"])
    
                elif not self.result_df.loc[self.result_df.author == node["label"]].empty:
                    node["title"] =  self.getInfoAuteur(node["label"])
                    node["color"] = "red"
                    node["size"] = 20
    
    
                else :
                    node["color"] = "orange"
                    node ["size"] =  30
                    node["title"] = self.getInfoPublication(node["label"])
            self.result = G_Dyn
            return         
        
        return
    
    def selectAuteurVenuePublicationFromKeyword(self,Keyword) :
        df = self.getTreeKeyword(Keyword)
        if df.empty :
            self.result_df = pd.merge(self.df_pub_Aut,self.df_pub_venu,on="id_publication").merge(self.df_pub_key,on="id_publication")
            self.result_df = pd.merge(self.result_df,self.df_pub,on='id_publication').merge(self.df_ven,on ='id_venue')
            self.result_df = self.result_df[['keyword','article_title','author','name_venue']]
            G = nx.from_pandas_edgelist(self.result_df,source="article_title",target= "name_venue")
            H = nx.from_pandas_edgelist(self.result_df,source="article_title",target="keyword")
            F = nx.from_pandas_edgelist(self.result_df,source="name_venue",target="keyword")
            C = nx.from_pandas_edgelist(self.result_df,source="name_venue",target="author")
            D = nx.from_pandas_edgelist(self.result_df,source="keyword",target="author")
            E = nx.from_pandas_edgelist(self.result_df,source="article_title",target="author")
            graphfinal = nx.compose(G,H)
            graphfinal = nx.compose(graphfinal,F)
            graphfinal = nx.compose(graphfinal,C)
            graphfinal = nx.compose(graphfinal,D)
            graphfinal = nx.compose(graphfinal,E)
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal)
            voisin = G_Dyn.get_adj_list();
            for node in G_Dyn.nodes :
                if not self.result_df.loc[self.result_df.article_title == node['label']].empty : 
                    node["title"] = self.getInfoPublication(node["label"])
                    node["color"] = "green"

                elif not self.result_df.loc[self.result_df.keyword == node["label"]].empty:
                    node["title"] = self.getInfoKeyword(node["label"])
                    node["size"] =10*len(voisin[node["id"]])
                    node["shape"] = "star"
    
                elif not self.result_df.loc[self.result_df.name_venue == node["label"]].empty: 
                    node["title"] = self.getInfoVenue(node["label"])
                    node["color"] = "blue"
                else:
                    node["color" ] = "red"
                    node["size"] = 20
                    node["title"] = self.getInfoAuteur(node["label"])
                    
            self.result = G_Dyn
            return 
        else :
            self.result_df = df[['keyword','article_title','author','name_venue']]
            G = nx.from_pandas_edgelist(self.result_df,source="article_title",target= "name_venue")
            H = nx.from_pandas_edgelist(self.result_df,source="article_title",target="keyword")
            F = nx.from_pandas_edgelist(self.result_df,source="name_venue",target="keyword")
            C = nx.from_pandas_edgelist(self.result_df,source="name_venue",target="author")
            D = nx.from_pandas_edgelist(self.result_df,source="keyword",target="author")
            E = nx.from_pandas_edgelist(self.result_df,source="article_title",target="author")
            graphfinal = nx.compose(G,H)
            graphfinal = nx.compose(graphfinal,F)
            graphfinal = nx.compose(graphfinal,C)
            graphfinal = nx.compose(graphfinal,D)
            graphfinal = nx.compose(graphfinal,E)
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal)
            voisin = G_Dyn.get_adj_list();
            for node in G_Dyn.nodes :
                if not self.result_df.loc[self.result_df.article_title == node['label']].empty : 
                    node["title"] = self.getInfoPublication(node["label"])
                    node["color"] = "green"

                elif not self.result_df.loc[self.result_df.keyword == node["label"]].empty:
                    node["title"] = self.getInfoKeyword(node["label"])
                    node["size"] =10*len(voisin[node["id"]])
                    node["shape"] = "star"

                elif not self.result_df.loc[self.result_df.name_venue == node["label"]].empty: 
                    node["title"] = self.getInfoVenue(node["label"])
                    node["color"] = "blue"
                else:
                    node["color" ] = "red"
                    node["size"] = 20
                    node["title"] = self.getInfoAuteur(node["label"])
                    
            self.result = G_Dyn
            return 
    
    def selectAuteurPublicationFromKeyword(self,Keyword):
        df = self.getTreeKeyword(Keyword)
        if df.empty :
            self.result_df = pd.merge(self.df_pub_key,self.df_pub,on='id_publication').merge(self.df_pub_Aut,on = 'id_publication')[['keyword','article_title','author']]
            C = nx.from_pandas_edgelist(self.result_df,source="article_title",target="author")
            F = nx.from_pandas_edgelist(self.result_df,source="author",target="keyword")
            G = nx.from_pandas_edgelist(self.result_df,source="article_title",target="keyword")            
            graphfinal = nx.compose(F,G)
            graphfinal = nx.compose(graphfinal,C)
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal)
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes :
                if not self.result_df.loc[self.result_df.keyword == node["label"]].empty:
                    node["shape"] = "star"
                    node ["size"] = len(voisin[node["id"]])*5
                    node["title"] = self.getInfoKeyword(node["label"])
    
                elif not self.result_df.loc[self.result_df.article_title == node["label"]].empty:
                    node["title"] =  self.getInfoPublication(node["label"])
                    node["color"] = "red"
                    node["size"] = 20
    
                else :
                    node["color"] = "orange"
                    node ["size"] =  30
                    node["title"] = self.getInfoAuteur(node["label"])
            self.result = G_Dyn                
            return
        else : 
            self.result_df = df[['keyword','article_title','author']]
            C = nx.from_pandas_edgelist(self.result_df,source="article_title",target="author")
            F = nx.from_pandas_edgelist(self.result_df,source="author",target="keyword")
            G = nx.from_pandas_edgelist(self.result_df,source="article_title",target="keyword")            
            graphfinal = nx.compose(F,G)
            graphfinal = nx.compose(graphfinal,C)
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal)
            for node in G_Dyn.nodes :
                if not self.result_df.loc[self.result_df.keyword == node["label"]].empty:
                    node["shape"] = "star"
                    node ["size"] =  30
                    node["title"] = self.getInfoKeyword(node["label"])
    
                elif not self.result_df.loc[self.result_df.article_title == node["label"]].empty:
                    node["title"] =  self.getInfoPublication(node["label"])
                    node["color"] = "red"

    
                else :
                    node["color"] = "orange"

                    node["title"] = self.getInfoAuteur(node["label"])
            self.result = G_Dyn                
            return
        
        return        
    
    def selectAuteurVenueFromKeyword(self,Keyword) :
        df = self.getTreeKeyword(Keyword)
        if df.empty :
            self.result_df = pd.merge(self.df_pub_key,self.df_pub_Aut,on='id_publication').merge(self.df_pub_venu,on = 'id_publication').merge(self.df_ven,on='id_venue')[['keyword','name_venue','author']]
            C = nx.from_pandas_edgelist(self.result_df,source="author",target="name_venue")
            F = nx.from_pandas_edgelist(self.result_df,source="name_venue",target="keyword")
            G = nx.from_pandas_edgelist(self.result_df,source="author",target="keyword")            
            graphfinal = nx.compose(F,G)
            graphfinal = nx.compose(graphfinal,C)
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal)
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes :
                if not self.result_df.loc[self.result_df.keyword == node["label"]].empty:
                    node["shape"] = "star"
                    node ["size"] = len(voisin[node["id"]])*5
                    node["title"] = self.getInfoKeyword(node["label"])
    
                elif not self.result_df.loc[self.result_df.author == node["label"]].empty:
                    node["title"] =  self.getInfoAuteur(node["label"])
                    node["color"] = "red"
                    node["size"] = 20
    
                else :
                    node["color"] = "orange"
                    node ["size"] =  30
                    node["title"] = self.getInfoVenue(node["label"])
            self.result = G_Dyn                
            return
        else : 
            self.result_df = df[['keyword','name_venue','author']]
            C = nx.from_pandas_edgelist(self.result_df,source="author",target="name_venue")
            F = nx.from_pandas_edgelist(self.result_df,source="name_venue",target="keyword")
            G = nx.from_pandas_edgelist(self.result_df,source="author",target="keyword")            
            graphfinal = nx.compose(F,G)
            graphfinal = nx.compose(graphfinal,C)
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal)
            for node in G_Dyn.nodes :
                if not self.result_df.loc[self.result_df.keyword == node["label"]].empty:
                    node["shape"] = "star"
                    node ["size"] =  30
                    node["title"] = self.getInfoKeyword(node["label"])
    
                elif not self.result_df.loc[self.result_df.author == node["label"]].empty:
                    node["title"] =  self.getInfoAuteur(node["label"])
                    node["color"] = "red"

    
                else :
                    node["color"] = "orange"

                    node["title"] = self.getInfoVenue(node["label"])
            self.result = G_Dyn                
            return
        
        return        
    
    def selectVenuePublicationFromKeyword(self,Keyword) :
        df = self.getTreeKeyword(Keyword)
        if df.empty :
            self.result_df = pd.merge(self.df_pub_key,self.df_pub,on='id_publication').merge(self.df_pub_venu,on = 'id_publication').merge(self.df_ven,on='id_venue')[['keyword','article_title','name_venue']]
            C = nx.from_pandas_edgelist(self.result_df,source="article_title",target="name_venue")
            F = nx.from_pandas_edgelist(self.result_df,source="name_venue",target="keyword")
            G = nx.from_pandas_edgelist(self.result_df,source="article_title",target="keyword")            
            graphfinal = nx.compose(F,G)
            graphfinal = nx.compose(graphfinal,C)
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal)
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes :
                if not self.result_df.loc[self.result_df.keyword == node["label"]].empty:
                    node["shape"] = "star"
                    node ["size"] = len(voisin[node["id"]])*5
                    node["title"] = self.getInfoKeyword(node["label"])
    
                elif not self.result_df.loc[self.result_df.article_title == node["label"]].empty:
                    node["title"] =  self.getInfoPublication(node["label"])
                    node["color"] = "red"
                    node["size"] = 20
    
                else :
                    node["color"] = "orange"
                    node ["size"] =  30
                    node["title"] = self.getInfoVenue(node["label"])
            self.result = G_Dyn                
            return
        else : 
            self.result_df = df[['keyword','article_title','name_venue']]
            C = nx.from_pandas_edgelist(self.result_df,source="article_title",target="name_venue")
            F = nx.from_pandas_edgelist(self.result_df,source="name_venue",target="keyword")
            G = nx.from_pandas_edgelist(self.result_df,source="article_title",target="keyword")            
            graphfinal = nx.compose(F,G)
            graphfinal = nx.compose(graphfinal,C)
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal)
            for node in G_Dyn.nodes :
                if not self.result_df.loc[self.result_df.keyword == node["label"]].empty:
                    node["shape"] = "star"
                    node ["size"] =  30
                    node["title"] = self.getInfoKeyword(node["label"])
    
                elif not self.result_df.loc[self.result_df.article_title == node["label"]].empty:
                    node["title"] =  self.getInfoPublication(node["label"])
                    node["color"] = "red"

    
                else :
                    node["color"] = "orange"

                    node["title"] = self.getInfoVenue(node["label"])
            self.result = G_Dyn                
            return
        
        return
    
    def selectVenueFromKeyword(self,Keyword):
        df = self.getTreeKeyword(Keyword)
        if df.empty :
            self.result_df = pd.merge(self.df_pub_key,self.df_pub_venu,on = 'id_publication').merge(self.df_ven,on ='id_venue')[["keyword","name_venue"]]
            G = nx.from_pandas_edgelist(self.result_df,source ="name_venue",target="keyword")
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(G)
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes : 
                if not self.result_df.loc[self.result_df.keyword == node["label"]].empty : 
                    node["color"] = "#dd4b39"
                    node["shape"] = "star"
                    node["size"] = 5 * len(voisin[node["id"]])
                    node["title"] = self.getInfoKeyword(node["label"]) 
                else :
                    node["size"] =  20
                    node["title"] = self.getInfoVenue(node["label"])
            self.result = G_Dyn
            return
        else : 
            self.result_df = df[["keyword","name_venue"]]
            G = nx.from_pandas_edgelist(self.result_df,source="keyword",target="name_venue")
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(G)

            for node in G_Dyn.nodes : 
                if not self.result_df.loc[self.result_df.keyword == node["label"]].empty : 
                    node["color"] = "#dd4b39"
                    node["shape"] = "star"
                    node["size"] =  20
                    node["title"] = self.getInfoKeyword(node["label"]) 
                else :

                    node["title"] = self.getInfoVenue(node["label"])
            self.result = G_Dyn
            return
    
    def selectAuteurFromKeyword(self,Keyword):
        df = self.getTreeKeyword(Keyword)
        if df.empty :
            self.result_df = pd.merge(self.df_pub_key,self.df_pub_Aut,on = 'id_publication')[["keyword","author"]]
            G = nx.from_pandas_edgelist(self.result_df,source ="author",target="keyword")
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(G)
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes : 
                if not self.result_df.loc[self.result_df.keyword == node["label"]].empty : 
                    node["color"] = "#dd4b39"
                    node["shape"] = "star"
                    node["size"] = 5 * len(voisin[node["id"]])
                    node["title"] = self.getInfoKeyword(node["label"]) 
                else :
                    node["size"] =  20
                    node["title"] = self.getInfoAuteur(node["label"])
            self.result = G_Dyn
            return
        else : 
            self.result_df = df[["keyword","author"]]
            G = nx.from_pandas_edgelist(self.result_df,source="keyword",target="author")
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(G)
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes : 
                if not self.result_df.loc[self.result_df.keyword == node["label"]].empty : 
                    node["color"] = "#dd4b39"
                    node["shape"] = "star"
                    node["size"] =  20
                    node["title"] = self.getInfoKeyword(node["label"]) 
                else :
                    node["title"] = self.getInfoAuteur(node["label"])
            self.result = G_Dyn
            return
                
    
    def selectPublicationFromKeyword(self,Keyword):
        df = self.getTreeKeyword(Keyword)
        if df.empty :
            self.result_df = pd.merge(self.df_pub_key,self.df_pub,on = 'id_publication')[["keyword","article_title"]]
            G = nx.from_pandas_edgelist(self.result_df,source ="article_title",target="keyword")
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(G)
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes : 
                if not self.result_df.loc[self.result_df.keyword == node["label"]].empty : 
                    node["color"] = "#dd4b39"
                    node["shape"] = "star"
                    node["size"] = 5 * len(voisin[node["id"]])
                    node["title"] = self.getInfoKeyword(node["label"]) 
                else :
                    node["size"] =  20
                    node["title"] = self.getInfoPublication(node["label"])
            self.result = G_Dyn
            return
        else : 
            self.result_df = df[["keyword","article_title"]]
            G = nx.from_pandas_edgelist(self.result_df,source="keyword",target="article_title")
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(G)
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes : 
                if not self.result_df.loc[self.result_df.keyword == node["label"]].empty : 
                    node["color"] = "#dd4b39"
                    node["shape"] = "star"
                    node["size"] =  20
                    node["title"] = self.getInfoKeyword(node["label"]) 
                else :
                    node["title"] = self.getInfoPublication(node["label"])
            self.result = G_Dyn
            return
        
        return
    
    def selectPublicationFromVenue(self,idVenue):
        df = self.getTreeVenue(idVenue)
        if df.empty:
             self.df_pub_venu.dropna(subset = ["id_venue"], inplace=True)
             G = nx.from_pandas_edgelist(self.df_pub_venu,source="id_venue",target="id_publication")
             G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
             G_Dyn.from_nx(G)
             voisin = G_Dyn.get_adj_list()
             for node in G_Dyn.nodes :
                if not self.df_pub_venu.loc[self.df_pub_venu.id_venue == node["label"]].empty : 
                    node["color"] = "#dd4b39"
                    node["shape"] = "star"
                    node["size"] = 5 * len(voisin[node["id"]])
                    node["title"] = self.getInfoVenue(node["label"])
                else :
                    node["title"] = self.getInfoPublication(node["label"])
                    node["color"] = "blue"
                    node["label"] = self.getTitle_from_pub(node["label"])
             self.result =  G_Dyn 
             self.result_df = self.df_pub_venu.merge(self.df_pub,on="id_publication").merge(self.df_ven,on ="id_venue")[['article_title','name_venue']].copy()
             return 
        else:
            self.result_df = df[['name_venue','article_title']]
            G = nx.from_pandas_edgelist(df,source="name_venue",target="article_title")
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(G)
            for node in G_Dyn.nodes :
                if not df.loc[df.name_venue == node["label"]].empty : 
                    node["color"] = "#dd4b39"
                    node["shape"] = "star"
                    node["size"] = 20
                    node["title"] = self.getInfoVenue(node["label"])
                else :
                    node["title"] = self.getInfoPublication(node["label"])
                    node["color"] = "blue"
            self.result = G_Dyn
            return 
    def selectkeywordFromVenue(self,idVenue):
        df = self.getTreeVenue(idVenue)
        if df.empty :
            self.result_df = pd.merge(self.df_pub_key,self.df_pub_venu,on = 'id_publication').merge(self.df_ven,on = 'id_venue')[['name_venue','keyword']]
            G = nx.from_pandas_edgelist(self.result_df,source = 'name_venue', target = 'keyword')
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(G)
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes :
                if not self.result_df.loc[self.result_df.name_venue == node["label"]].empty :
                    node["color"] = "#dd4b39"
                    node["shape"] = "star"
                    node["title"] = self.getInfoVenue(node["label"])
                    node["size"] = 30
                else :
                    node["title"] = self.getInfoKeyword(node["label"])
                    node["color"] = "blue"
                    node["size"] = 5 * len(voisin[node["id"]])
            self.result = G_Dyn
            return
        else:
            self.result_df = df[['name_venue','keyword']]
            G = nx.from_pandas_edgelist(self.result_df,source = 'name_venue', target = 'keyword')
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(G)
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes :
                if not self.result_df.loc[self.result_df.name_venue == node["label"]].empty :
                    node["color"] = "#dd4b39"
                    node["shape"] = "star"
                    node["title"] = self.getInfoVenue(node["label"])
                    node["size"] = 30
                else :
                    node["title"] = self.getInfoKeyword(node["label"])
                    node["color"] = "blue"
                    node["size"] = 10
            self.result = G_Dyn
            return           
    
    def selectCoAuteurFromAuteur(self, idAuteur):
        #selectionner les publications de l'auteur rechrché 
        df_pub_auteur = self.df_pub_Aut.loc[self.df_pub_Aut.author == idAuteur]
        if df_pub_auteur.empty :
            df= pd.read_csv("data/publication_author.csv",sep=";",nrows=self.maxLimit)
            G = nx.from_pandas_edgelist(df,source="id_publication",target="author")
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(G)
            voisin = G_Dyn.get_adj_list();
            for node in G_Dyn.nodes :
                if not self.df_pub.loc[self.df_pub.id_publication == node['label']].empty : 
                    node["title"] = self.getInfoPublication(node["label"])
                    node["color"] = "#dd4b39"
                else :
                    node["title"] = self.getInfoAuteur(node["label"])
                    node["size"] =10*len(voisin[node["id"]])
                    node["shape"] = "star"
            self.result = G_Dyn
            self.result_df = df
            return
        #selectionner les autres auteurs ayant les publciation de l'auteur recherché
        is_co_auteur = self.df_pub_Aut.id_publication.isin(df_pub_auteur.id_publication)
        
        # jointeur entre le deux tables pour avoir les noms des auteurs 
        df_co_auteur=pd.merge(self.df_pub_Aut[is_co_auteur],self.df_aut, left_on='author', right_on='author',suffixes=('_left', '_right'), how='inner')
        df_co_auteur = df_co_auteur[['author']].drop_duplicates()
        df_co_auteur = df_co_auteur.assign(source=idAuteur)
        df_co_auteur = df_co_auteur.loc[df_co_auteur.author!=idAuteur]
        G = nx.from_pandas_edgelist(df_co_auteur,source="source",target="author")
        G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
        G_Dyn.from_nx(G)
        for node in G_Dyn.nodes :
            node["title"] = self.getInfoAuteur(node["label"])
            if node["label"] == idAuteur :
                node["color"] = "#dd4b39"
                node ["size"] = 20
                node["shape"] = "star"
        self.result = G_Dyn
        self.result_df = df_co_auteur
        return
        
    #Affichage des mots-clés utilisés par un auteur donné

    def selectMotsClesFromAuteur(self, idAuteur):
        df_pub_auteur = self.df_pub_Aut.loc[self.df_pub_Aut.author == idAuteur]
        if  df_pub_auteur.empty :
            df = self.df_pub_Aut.merge(self.df_pub_key,on="id_publication").head(self.maxLimit)
            G = nx.from_pandas_edgelist(df,source="author",target="keyword")
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(G)
            voisin = G_Dyn.get_adj_list();
            for node in G_Dyn.nodes :
                if not self.df_key.loc[self.df_key.keyword == node["label"]].empty :
                    node["title"] =  self.getInfoKeyword(node["label"])
                else :
                    node["color"] = "#dd4b39"
                    node ["size"] = len(voisin[node["id"]])*5
                    node["title"] = self.getInfoAuteur(node["label"])
                    node["shape"] = "star"
                    
            self.result = G_Dyn
            self.result_df = df
            return
        else :
            df_keyword_auteur = self.getTreeAuteur(idAuteur)
            G = nx.from_pandas_edgelist(df_keyword_auteur,source="author",target="keyword")
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(G)
            for node in G_Dyn.nodes :
                if not self.df_key.loc[self.df_key.keyword == node["label"]].empty:
                    node["title"] =  self.getInfoKeyword(node["label"])
                else :
                    node["color"] = "#dd4b39"
                    node ["size"] = 20
                    node["title"] = self.getInfoAuteur(node["label"])
                    node["shape"] = "star"
            self.result = G_Dyn
            self.result_df = df_keyword_auteur
            return
    def selectVenueParAuteur(self,idAuteur):
        df = self.df_aut.loc[self.df_aut.author == idAuteur]
        if df.empty : 
            self.result_df = self.df_pub_Aut
            self.result_df = pd.merge( self.result_df,self.df_pub_venu,on="id_publication")
            self.result_df = pd.merge( self.result_df,self.df_ven,on="id_venue")
            G = nx.from_pandas_edgelist( self.result_df,source="author",target="name_venue")
            G_Dyn =  Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(G)
            voisin = G_Dyn.get_adj_list()
            for node in  G_Dyn.nodes :
                if not  self.result_df.loc[self.result_df.author == node["label"]].empty :
                    node["color"] = "orange"
                    node["shape"] = "star"
                    node ["size"] = len(voisin[node["id"]])*5
                    node["title"] = self.getInfoAuteur(node["label"])     
                else :
                    node["title"] =  self.getInfoVenue(node["label"])
                    node["color"] = "blue"             
            self.result = G_Dyn
            return
        else :
            self.result_df  = self.df_pub_Aut.loc[self.df_pub_Aut.author == idAuteur]
            self.result_df = pd.merge( self.result_df,self.df_pub_venu,on="id_publication")
            self.result_df = pd.merge( self.result_df,self.df_ven,on="id_venue")
            G = nx.from_pandas_edgelist( self.result_df,source="author",target="name_venue")
            G_Dyn =  Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(G)
            for node in  G_Dyn.nodes :
                if not  self.result_df.loc[ self.result_df.author == node["label"]].empty :
                    node["color"] = "orange"
                    node["shape"] = "star"
                    node ["size"] = 30
                    node["title"] = self.getInfoAuteur(node["label"])     
                else :
                    node["title"] =  "Venue"
                    node["color"] = "blue"             
            self.result =  G_Dyn
        return
    #Affichage des publications d'un auteur donné
          
    def selectPublicationFromAuteur(self, idAuteur):
        df_pub_auteur = self.df_pub_Aut.loc[self.df_pub_Aut.author == idAuteur]
        if  df_pub_auteur.empty :
            df= pd.read_csv("data/publication_author.csv",sep=";",nrows=self.maxLimit)
            G = nx.from_pandas_edgelist(df,source="author",target="id_publication")
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(G)
            voisin = G_Dyn.get_adj_list();
            for node in G_Dyn.nodes :
                if not self.df_pub.loc[self.df_pub.id_publication == node['label']].empty : 
                    node["title"] = self.getInfoPublication(node["label"])
                    node["label"] = self.getTitle_from_pub(node["label"])
                else :
                    node["title"] = self.getInfoAuteur(node["label"])
                    node["color"] = "#dd4b39"
                    node["size"] =10*len(voisin[node["id"]])
                    node["shape"] = "star"
            self.result = G_Dyn
            self.result_df = df
            return
        else :

            self.df_result =self.getTreeAuteur(idAuteur)
            self.df_result =self.df_result[['author','id_publication']].copy()
            G = nx.from_pandas_edgelist(self.df_result,source="author",target="id_publication")
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(G)
            for node in G_Dyn.nodes :
                if not self.df_pub.loc[self.df_pub.id_publication == node['label']].empty : 
                    node["title"] = self.getInfoPublication(node["label"])
                    node["label"] = self.getTitle_from_pub(node["label"])
                else :
                    node["title"] = self.getInfoAuteur(node["label"])
                    node["color"] = "#dd4b39"
                    node["size"] =20
                    node["shape"] = "star"
            self.result = G_Dyn

            return
            
    
    
    def selectCoauteurPublicationFromAuteur(self,idAuteur):
        df = self.df_aut.loc[self.df_aut.author == idAuteur]
        if df.empty : 
            return self.selectCoAuteurFromAuteur('')
        else : 
             
            df = self.getTreeAuteur(idAuteur)
            G = nx.from_pandas_edgelist(df,source="author",target="id_publication")
            F = nx.from_pandas_edgelist(df,source="author",target="name_co_author")
            H = nx.from_pandas_edgelist(df,source="id_publication",target="name_co_author")
            graphfinal = nx.compose(G,F)
            graphfinal = nx.compose(graphfinal,H)
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal)            
            for node in G_Dyn.nodes :
                if not self.df_pub.loc[self.df_pub.id_publication == node['label']].empty : 
                    node["title"] = self.getInfoPublication(node["label"])
                    node["label"] = self.getTitle_from_pub(node["label"])
                    node["color"] = "orange"
                else : 
                    if node["label"] == idAuteur : 
                        node["size"] = 30
                        node["color"] = "green"
                    else : 
                        node["size"] = 20
                        node["color"] = "red"
                        
                    node["title"] = self.getInfoAuteur(node["label"])
                    node["shape"] = "star"
            
                    
            self.result = G_Dyn
            #AVANT DE RETOURNER LE RESULTAT COMME LE DF EST TOUT LARBRE DE LAUTEUR FAUDRAIT LAISSER QUE LES COLOGNES EN QUESTIONS 
            
            self.result_df = df[['id_publication','author','name_co_author']]           
            
            return 
    def selectKeywordPublicationFromVenue(self,idVenue):
        df = self.getTreeVenue(idVenue)
        if  df.empty :
            self.result_df = self.df_pub_venu.merge(self.df_pub_key,on="id_publication")
            self.result_df = self.result_df.merge(self.df_pub,on="id_publication").merge(self.df_ven,on='id_venue')
            self.result_df = self.result_df[['name_venue','article_title','keyword']]
            G = nx.from_pandas_edgelist(self.result_df,source="name_venue",target="keyword")
            H= nx.from_pandas_edgelist(self.result_df,source="name_venue",target="article_title")
            E=nx.from_pandas_edgelist(self.result_df,source="article_title",target="keyword")
            graphfinal=nx.compose(G,H)
            graphfinal=nx.compose(graphfinal,E)
    
    
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal)
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes :
  
                if not self.result_df[self.result_df.name_venue == node["label"]].empty:
                    node["color"] = "#dd4b39"
                    node["shape"] = "star"
                    node ["size"] = len(voisin[node["id"]])*5
                    node["title"] = self.getInfoVenue(node["label"])
    
                elif not self.result_df.loc[self.result_df.keyword == node["label"]].empty:
                    node["title"] =  self.getInfoKeyword(node["label"])
                    node["color"] = "blue"
    
    
                else :
                    node["color"] = "orange"
                    node["shape"] = "star"
                    node ["size"] =  30
                    node["title"] = self.getInfoPublication(node["label"])
            self.result = G_Dyn
            return
        else :
            self.result_df = df [['name_venue','article_title','keyword']]
            C = nx.from_pandas_edgelist(self.result_df,source="article_title",target="name_venue")
            F = nx.from_pandas_edgelist(self.result_df,source="name_venue",target="keyword")
            G = nx.from_pandas_edgelist(self.result_df,source="article_title",target="keyword")
            graphfinal = nx.compose(F,G)
            graphfinal = nx.compose(graphfinal,C)
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal)
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes :
    
                if not self.result_df.loc[self.result_df.name_venue == node["label"]].empty:
    
    
                    node["color"] = "#dd4b39"
                    node["shape"] = "star"
                    node ["size"] = len(voisin[node["id"]])*5
                    node["title"] = self.getInfoVenue(node["label"])
    
                elif not self.result_df.loc[self.result_df.keyword == node["label"]].empty:
                    node["title"] =  "KEYWRD"
                    node["color"] = "blue"
    
    
                else :
                    node["color"] = "orange"
                    node ["size"] =  30
                    node["title"] = self.getInfoPublication(node["label"])
            self.result = G_Dyn
            return 
    
    
    
    def selectMotsClesAndPublicationsFromAuteur(self,idAuteur):
        df_pub_auteur = self.df_pub_Aut.loc[self.df_pub_Aut.author == idAuteur]
        if  df_pub_auteur.empty :
            df = self.df_pub_Aut.merge(self.df_pub_key,on="id_publication")
            df = df.merge(self.df_pub,on="id_publication")
            self.result_df = df[['author','id_publication','keyword']].copy()
            G = nx.from_pandas_edgelist(df,source="author",target="keyword")
            H= nx.from_pandas_edgelist(df,source="author",target="id_publication")
            E=nx.from_pandas_edgelist(df,source="id_publication",target="keyword")
            graphfinal=nx.compose(G,H)
            graphfinal=nx.compose(graphfinal,E)
    
    
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal)
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes :
  
                if not self.df_pub_Aut.loc[self.df_pub_Aut.id_publication == node["label"]].empty:
                    node["color"] = "#dd4b39"
                    #node["shape"] = "triangle"
                    node ["size"] = len(voisin[node["id"]])*2
                    node["title"] = self.getInfoPublication(node["label"])
                    node["label"] = self.getTitle_from_pub(node["label"])
    
                elif not self.df_pub_key.loc[self.df_pub_key.keyword == node["label"]].empty:
                    node["title"] =  self.getInfoKeyword(node["label"])
                    node["color"] = "blue"
    
    
                else :
                    node["color"] = "orange"
                    node["shape"] = "star"
                    node ["size"] = len(voisin[node["id"]])*2
                    node["title"] = self.getInfoAuteur(node["label"])
            self.result = G_Dyn
            return
        else :
            self.result_df = self.getTreeAuteur(idAuteur)[['author','id_publication','keyword']].copy()
            C = nx.from_pandas_edgelist(self.result_df,source="id_publication",target="author")
            F = nx.from_pandas_edgelist(self.result_df,source="author",target="keyword")
            G = nx.from_pandas_edgelist(self.result_df,source="id_publication",target="keyword")
            graphfinal = nx.compose(F,G)
            graphfinal = nx.compose(graphfinal,C)
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal)
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes :
    
                if not self.df_pub_Aut.loc[self.df_pub_Aut.id_publication == node["label"]].empty:
    
    
                    node["color"] = "#dd4b39"
                    #node["shape"] = "triangle"
                    node ["size"] = len(voisin[node["id"]])*5
                    node["title"] = self.getInfoPublication(node["label"])
                    node["label"] = self.getTitle_from_pub(node["label"])
    
                elif not self.df_pub_key.loc[self.df_pub_key.keyword == node["label"]].empty:
                    node["title"] =  "KEYWRD"
                    node["color"] = "blue"
    
    
                else :
                    if node["label"] in idAuteur :
                        node["color"] = "green"
                        node["shape"] = "star"
                        node ["size"] = 20
                        node["title"] = self.getInfoAuteur(node["label"])
                    else :
                        node["color"] = 'orange'
                        node["shape"] = "star"
                        node["title"] = self.getInfoAuteur(node["label"])
            self.result = G_Dyn
           
            return 
        
    def selectCoauteurVenueFromAuteur(self,idAuteur) :
        df = self.df_aut.loc[self.df_aut.author == idAuteur]
        if df.empty : 
            self.result_df = pd.merge(self.df_pub_Aut,self.df_pub_Aut,on="id_publication")
            self.result_df = pd.merge(self.result_df,self.df_pub_venu,on="id_publication")
            self.result_df = pd.merge(self.result_df,self.df_ven,on="id_venue")
            self.result_df = self.result_df.rename(columns = {'author_x' : 'author','author_y' : 'co_auteur'})
            self.result_df = self.result_df.loc[self.result_df.author != self.result_df.co_auteur]
            self.result_df = self.result_df[['author','co_auteur','name_venue']].copy()
            G = nx.from_pandas_edgelist(self.result_df,source = 'author',target = 'co_auteur')
            H = nx.from_pandas_edgelist(self.result_df,source = 'author',target = 'name_venue')
            F = nx.from_pandas_edgelist(self.result_df,source = 'name_venue',target = 'co_auteur')
            graphfinal = nx.compose(G,H)
            graphfinal = nx.compose(graphfinal,F)
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal)
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes :
                if not self.df_aut.loc[self.df_aut.author == node["label"]].empty :
                        node["color"] = "orange"
                        node["shape"] = "star"
                        node ["size"] =  5 * len(voisin[node["id"]])
                        node["title"] = self.getInfoAuteur(node["label"])                    
                else:
                        node["color"] = "purple"
                        node ["size"] =  20
                        node["title"] = self.getInfoVenue(node["label"])                      
            self.result = G_Dyn
            return
        else : 
            self.result_df = self.getTreeAuteur(idAuteur)[['author','name_co_author','name_venue']].copy()
            self.result_df.drop_duplicates(keep='first',inplace= False, subset='name_co_author')
            G = nx.from_pandas_edgelist(self.result_df,source = 'author',target = 'name_co_author')
            H = nx.from_pandas_edgelist(self.result_df,source = 'author',target = 'name_venue')
            F = nx.from_pandas_edgelist(self.result_df,source = 'name_venue',target = 'name_co_author')
            graphfinal = nx.compose(G,H)
            graphfinal = nx.compose(graphfinal,F)
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal)
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes :
                if not self.df_aut.loc[self.df_aut.author == node["label"]].empty :
                        if node["label"] in idAuteur : 
                            node["color"] = "green"
                        else :
                            node["color"] = "orange"
                            

                        node["shape"] = "star"
                        node ["size"] =  5 * len(voisin[node["id"]])
                        node["title"] = self.getInfoAuteur(node["label"])                    
                else:
                        node["color"] = "purple"
                        node ["size"] =  20
                        node["title"] = self.getInfoVenue(node["label"])                      
            self.result = G_Dyn            
            return 
        
    def selectCoauteurPublicationKeywordFromAuteur(self,idAuteur):
        df = self.df_aut.loc[self.df_aut.author == idAuteur]
        if df.empty :
            self.result_df = pd.merge(self.df_pub_Aut,self.df_pub_Aut,on="id_publication")
            self.result_df = pd.merge(self.result_df,self.df_pub_key,on="id_publication")
            self.result_df = self.result_df.rename(columns = {'author_x' : 'author','author_y' : 'co_auteur'})
            self.result_df = self.result_df.loc[self.result_df.author != self.result_df.co_auteur]
            self.result_df = self.result_df.merge(self.df_pub,on ='id_publication')
            self.result_df = self.result_df[['author','article_title','co_auteur','keyword']]
            G = nx.from_pandas_edgelist(self.result_df,source="article_title",target="author")
            H = nx.from_pandas_edgelist(self.result_df,source="article_title",target="keyword")
            F = nx.from_pandas_edgelist(self.result_df,source="author",target="keyword")
            C = nx.from_pandas_edgelist(self.result_df,source="author",target="co_auteur")
            D = nx.from_pandas_edgelist(self.result_df,source="keyword",target="co_auteur")
            E = nx.from_pandas_edgelist(self.result_df,source="article_title",target="co_auteur")
            graphfinal = nx.compose(G,H)
            graphfinal = nx.compose(graphfinal,F)
            graphfinal = nx.compose(graphfinal,C)
            graphfinal = nx.compose(graphfinal,D)
            graphfinal = nx.compose(graphfinal,E)
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal)
            voisin = G_Dyn.get_adj_list();
            for node in G_Dyn.nodes :
                if not self.result_df.loc[self.result_df.article_title == node['label']].empty : 
                    node["title"] = self.getInfoPublication(node["label"])
                    node["color"] = "#dd4b39"

                elif not self.result_df.loc[self.result_df.author == node["label"]].empty or not self.result_df.loc[self.result_df.co_auteur == node["label"]].empty :
                    node["title"] = self.getInfoAuteur(node["label"])
                    node["size"] =10*len(voisin[node["id"]])
                    node["shape"] = "star"
                    node["color"] = "green"
                else :
                    node["title"] = self.getInfoKeyword(node["label"])
                    node["color"] = "blue"
            self.result = G_Dyn
            return
        
        else :
            self.result_df = self.getTreeAuteur(idAuteur)[['author','id_publication','keyword']].copy()
            G = nx.from_pandas_edgelist(self.result_df,source="id_publication",target="author")
            H = nx.from_pandas_edgelist(self.result_df,source="id_publication",target="keyword")
            graphfinal = nx.compose(G,H)
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal)
            for node in G_Dyn.nodes :
                if not self.df_pub.loc[self.df_pub.id_publication == node['label']].empty : 
                    node["title"] = self.getInfoPublication(node["label"])
                    node["color"] = "#dd4b39"
                    node["label"] = self.getTitle_from_pub(node["label"])
                elif not self.df_aut.loc[self.df_aut.author == node["label"]].empty :
                    if (idAuteur in node["label"]):
                        node["color"] = "green"
                    else :
                        node["color"] = "red"
                    node["title"] = self.getInfoAuteur(node["label"])
                    node["size"] =30 
                    node["shape"] = "star"

                else :
                    node["title"] = self.getInfoKeyword(node["label"])
                    node["color"] = "blue"
            self.result = G_Dyn            
            return
       
    
    def selectCoauteurKeywordFromAuteur(self,idAuteur) :
        df = self.df_aut.loc[self.df_aut.author == idAuteur]
        if df.empty :         
            self.result_df = pd.merge(self.df_pub_Aut,self.df_pub_Aut,on="id_publication")
            self.result_df = pd.merge(self.result_df,self.df_pub_key,on="id_publication")
            self.result_df = self.result_df.rename(columns = {'author_x' : 'author','author_y' : 'co_auteur'})
            self.result_df = self.result_df[['author','co_auteur','keyword']].copy()
            self.result_df = self.result_df.loc[self.result_df.author != self.result_df.co_auteur]
            G = nx.from_pandas_edgelist(self.result_df,source="author",target="co_auteur")
            H = nx.from_pandas_edgelist(self.result_df,source="author",target="keyword")
            F = nx.from_pandas_edgelist(self.result_df,source="co_auteur",target="keyword")
            graphfinal = nx.compose(G,H)
            graphfinal = nx.compose(graphfinal,F)
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal)
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes :
                if not self.result_df.loc[self.result_df.author == node["label"]].empty :
                    node["color"] = "green"
                    node["shape"] = "star"
                    node["size"] = 30
                    node["title"] = self.getInfoAuteur(node["label"])
                else :
                    node["color"] ="orange"
                    node["size"] = len(voisin[node["id"]])*5
                    node["title"] = self.getInfoKeyword(node["label"])                    
                    
            self.result = G_Dyn
            return
        else :
            self.result_df = self.getTreeAuteur(idAuteur)[['author','name_co_author','keyword']].copy()
            G = nx.from_pandas_edgelist(self.result_df,source="author",target="name_co_author")
            H = nx.from_pandas_edgelist(self.result_df,source="author",target="keyword")
            F = nx.from_pandas_edgelist(self.result_df,source="name_co_author",target="keyword")
            graphfinal = nx.compose(G,H)
            graphfinal = nx.compose(graphfinal,F)
            G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
            G_Dyn.from_nx(graphfinal)
            voisin = G_Dyn.get_adj_list()
            for node in G_Dyn.nodes :
                if not self.df_aut.loc[self.df_aut.author == node["label"]].empty :
                    if idAuteur in node["label"]:
                        node["color"] = "green"
                    else : 
                        node["color"] = "red"
                    node["shape"] = "star"
                    node["size"] = 30
                    node["title"] = self.getInfoAuteur(node["label"])
                else :
                    node["color"] ="orange"
                    node["size"] = len(voisin[node["id"]])*5
                    node["title"] = self.getInfoKeyword(node["label"])                    
                    
            self.result = G_Dyn            
            return 
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    #Affichage des auteurs d'une publication donnée  

            
    #Affichage des lieux de publication pour un auteur donné  
    
    #def selectLieuxPublicationFromAuteur(self,idAuteur):
    #    df_pub_auteur = self.df_pub_Aut.loc[self.df_pub_Aut.id_author == idAuteur]
    #    if  df_pub_auteur.empty :
    #        df = self.df_pub_venu.merge(self.df_ven,on="id_venue")
    #        df = df.merge(self.df_pub_Aut,on="id_publication")
    #        df = df.loc[:, ~df.columns.str.contains('^Unnamed')].head(self.maxLimit)
    #        G = nx.from_pandas_edgelist(df,source="id_author",target="name_venue")
    #        G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
    #        G_Dyn.from_nx(G)
    #        for node in G_Dyn.nodes :
    #            if "id_" in node["label"] :
    #                node["title"] = self.getInfoAuteur(node["label"]) 
    #                node["color"] = "#dd4b39"
    #               
    #            else : 
    #                node["title"] = "VENUE"
    #                node["size"] =  20
    #        self.result = G_Dyn
    #        self.result_df = df
    #        return
    #        
    #         
    #    else :
    #        is_pub_auteur = self.df_pub_venu.id_publication.isin(df_pub_auteur.id_publication)
    #        df_liux_pub=pd.merge(self.df_pub_venu[is_pub_auteur],self.df_ven, left_on='id_venue', right_on='id_venue',suffixes=('_left', '_right'), how='inner')
    #        df =  df_liux_pub[['id_venue','name_venue','type_venue']].drop_duplicates().assign(source=idAuteur)
    #        G = nx.from_pandas_edgelist(df,source="source",target="name_venue")
    #        G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
    #        G_Dyn.from_nx(G)
    #
    #        for node in G_Dyn.nodes :
    #            if "id_" in node["label"] :
    #                node["title"] = self.getInfoAuteur(node["label"]) 
    #                node["color"] = "#dd4b39"
    #                node["size"] = 20
    #            else : 
    #                node["title"] = "VENUE"
    #        self.result = G_Dyn
    #        self.result_df = df
            
            
    #Affichage des 10 mots-clés les plus utilisés pour un lieu de publication donné
    #def selectTopMotsClesParLieux(self, idVenue):
    #    df_pub_venue = self.df_pub_venu.loc[self.df_pub_venu.id_venue == idVenue]
    #    if df_pub_venue.empty :
    #        df = self.df_pub_key.head(self.maxLimit)
    #        G = nx.from_pandas_edgelist(df,source="id_publication",target="keyword")
    #        G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
    #        G_Dyn.from_nx(G)
    #        voisin = G_Dyn.get_adj_list();
    #        for node in G_Dyn.nodes :
    #            if  node["label"].find("conf/") != -1:
    #                node["title"] = self.getInfoPublication(node["label"]) 
    #                node["color"] = "#dd4b39"
    #                node["size"] = 2* len(voisin[node["id"]])
    #            else : 
    #                node["title"] = "KEYWRD"
    #
    #        self.result = G_Dyn
    #        self.result_df = df
    #        return
    #    else :
    #        is_pub_key = self.df_pub_key.id_publication.isin(df_pub_venue.id_publication)
    #        df_keywords = pd.merge(self.df_pub_key[is_pub_key],self.df_key, left_on='keyword', right_on='keyword',suffixes=('_left', '_right'), how='inner')
    #        df_keywords = df_keywords['keyword'].value_counts().head(10)
    #        df=  df_keywords.to_frame().assign(source=idVenue) 
    #        df = df.reset_index()
    #        G = nx.from_pandas_edgelist(df,source="source",target="index")
    #        G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
    #        G_Dyn.from_nx(G)
    #        
    #        for node in G_Dyn.nodes :
    #            if  node["label"].find("id_") != -1:
    #                node["title"] = self.getInfoVenue(node["label"]) 
    #                node["color"] = "#dd4b39"
    #                node["size"] = 20
    #            else : 
    #                node["title"] = "KEYWRD = " + node["label"]
    #
    #        self.result = G_Dyn
    #        self.result_df = df_keywords
            
            
    # Les keywords les plus utilisés dans une année
    #def selectTopMotsClesParAnnee(self,idAnnee):
    #    df_pub_year_idAnnee = self.df_pub_year.loc[self.df_pub_year.id_year == idAnnee]
    #    if df_pub_year_idAnnee.empty :
    #        return
    #    is_pub_key = self.df_pub_key.id_publication.isin(df_pub_year_idAnnee.id_publication)
    #    df_keywords = pd.merge(self.df_pub_key[is_pub_key],self.df_key, left_on='keyword', right_on='keyword',suffixes=('_left', '_right'), how='inner')
    #    df_keywords = df_keywords['keyword'].value_counts().head(self.maxLimit)
    #    df_keywords = df_keywords.to_frame().assign(source=idAnnee)
    #    df_keywords = df_keywords.reset_index()
    #    G = nx.from_pandas_edgelist(df_keywords,source="source",target="index")
    #    G_Dyn = Network(height="750px", width="100%", bgcolor="#222222", font_color="white")
    #    G_Dyn.from_nx(G)
    #        
    #    for node in G_Dyn.nodes :
    #        if  node["label"].find("id_") != -1:
    #            node["title"] = node["label"].replace("id_","")
    #            node["color"] = "#dd4b39"
    #            node["size"] = 20
    #        else : 
    #            node["title"] ="KEYWRD=" +str(node["label"])
    #    self.result = G_Dyn
    #    self.result_df = df_keywords